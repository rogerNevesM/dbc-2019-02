public class TradutorParaIngles implements Tradutor{
    
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim":
                return "yes";
            case "Obrigado":
            case "Obrigada":
                return "Thank You";
            default:
                return null;
        }
    }
    
}
