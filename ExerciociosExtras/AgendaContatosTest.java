

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    @Test
    public void adicionarContato(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionarContato("roger","355553");
        assertEquals(1,contatos.getHash().size());
    }
    
    @Test
    public void buscarContato(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionarContato("roger","355553");
        assertEquals("355553",contatos.consultar("roger"));
    }
    
    @Test
    public void buscarNomePorTelefone(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionarContato("roger","355553");
        assertEquals("roger",contatos.consutarTelefone("355553"));
    }
    
    @Test
    public void retornandoCsv(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionarContato("roger","355553");
        contatos.adicionarContato("anice","567643");
        String separador = System.lineSeparator();
        String esperado = String.format("roger, 355553%sanice, 567643%s",separador,separador);
        assertEquals(esperado,contatos.retornarCsv());
    }
}
