import java.util.*;

public class AgendaContatos{
    private HashMap<String, String> contatos;
    
    public AgendaContatos(){
        contatos = new LinkedHashMap();
    }
    
    public void adicionarContato(String nome, String numero){
        contatos.put(nome,numero);
    }
    
    public HashMap<String, String> getHash(){
        return contatos;
    }
    
    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    
    public String consutarTelefone(String telefone){
        for(HashMap.Entry<String,String> par: contatos.entrySet()){
            if(par.getValue().equals(telefone)){
                return par.getKey();
            }
        }
        return null;
    }
    
    public String retornarCsv(){
        StringBuilder csv = new StringBuilder();
        String separator = System.lineSeparator();
        for(HashMap.Entry<String,String> par: contatos.entrySet()){
            String chave = par.getKey();
            String valor = par.getValue();
            String formato = String.format("%s, %s%s",chave,valor,separator);
            csv.append(formato);
        }
        return csv.toString();
    }
}
