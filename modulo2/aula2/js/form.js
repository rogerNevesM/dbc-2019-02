function validarCampos(event) {
    let nome = document.getElementById('nome');
    let email = document.getElementById('email');
    let assunto = document.getElementById('assunto');
    let mensagem = document.getElementById('mensagem');
    
    if (!nome.value || nome.value.length <10){
        event.preventDefault();
        nome.style.borderColor = "#f00";
    }else
    if(!email.value || !email.value.includes('@')){
        event.preventDefault();
        email.style.borderColor = "#f00";
    }else
    if(!assunto.value){
        event.preventDefault();
        assunto.style.borderColor = "#f00";
    }else
    if(!mensagem.value){
        event.preventDefault();
        mensagem.style.borderColor = "#f00";
    }

}
function validarNome() {
    console.log(this.value);
    if (this.value.length <10){
        this.style.borderColor = "#f00";
    }
}
function validarEmail() {
    if(!this.value.includes('@')){
        this.style.borderColor = "#f00";
    }
}

function voltarBorda() {
    this.style.borderColor = '#666';
}

function addEvento(){

    let button = document.getElementById('button');
    button.addEventListener('click',validarCampos);
    let nome = document.getElementById('nome');
    nome.addEventListener('blur',validarNome);
    let email = document.getElementById('email');
    email.addEventListener('blur',validarEmail);

    let assunto = document.getElementById('assunto');
    let mensagem = document.getElementById('mensagem');

    nome.addEventListener('focus',voltarBorda);
    email.addEventListener('focus',voltarBorda);
    assunto.addEventListener('focus',voltarBorda);
    mensagem.addEventListener('focus',voltarBorda);
}

addEventListener('load',addEvento);