//exercicio 1
function calculaCirculo({ raio, tipoCalculo }) {
    switch (tipoCalculo) {
        case "A":
            return Math.PI * Math.pow(raio, 2);
        case "C":
            return Math.PI * 2 * raio;
        default:
            return 0;
    }
}

let circulo = {
    raio: 5,
    tipoCalculo: "A"
}
console.log(calculaCirculo(circulo));

//exercicio 2
/*function naoBissexto(ano) {
    return (ano % 400 == 0 || (ano % 4 == 0 && ano % 100 != 0))?false:true;
}*/

let naoBissexto = ano => (ano % 400 == 0 || (ano % 4 == 0 && ano % 100 != 0)) ? false : true;

/* const testes = {
    diaAula:"segundo",
    local:"DBC",
    naoBissexto(ano){
    return (ano % 400 == 0 || (ano % 4 == 0 && ano % 100 != 0))?false:true;
    }
} */
console.log(naoBissexto(1900));

//Exercicio 3 
function somarPares(numeros) {
    let = soma = 0;
    console.log(numeros.length)
    for (let index = 0; index < numeros.length; index += 2) {
        soma += numeros[index];
    }
    return soma;
}

let numeros = [1, 5, 3, 6, 23.3, 55.7];
console.log(somarPares(numeros));

//Exercicio 4

let adicionar = num => add = n => num + n;

console.log(adicionar(234)(8));

//currying
const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(22));
console.log(is_divisivel(23));

//Exercicio 5 
/* let imprimirBRL = function(dinheiro){
    let negativo = false;
    if(dinheiro<0){
        negativo = true;
        dinheiro *=-1;
    }
    let [reais,centavos] = dinheiro.toString().split(".");
    if(!centavos){
        centavos = "00";
    }
    if(centavos.length<2){
        centavos+="00";
    }
    if(centavos.length >= 3){
        centavos = centavos.split("");
        if(centavos[2] && centavos[2]>0){
            centavos[1]++;
        }
        centavos = +centavos[0]+centavos[1].toString();
    }
    if(reais.length>3){
        reais = reais.split("");
        reais.reverse();
        let reaisNovo = [];
        for (let index = 0; index < reais.length; index++) {
            reaisNovo.push(reais[index]);
            if(index % 3 ==2){
                reaisNovo.push(".");
            }
        }
        reaisNovo.reverse();
        reais = reaisNovo.join('');
    }
    if(negativo){
        return `-R$ ${reais},${centavos}`;
    }
    return `R$ ${reais},${centavos}`;
    
} */
function arredondar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao);
    return Math.ceil(numero * fator) / fator;
}

function imprimirBRL(numero) {
    let qtdCasaMilhares = 3;
    let separadorMilhar = '.';
    let separadorDecimal = ',';

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero) % 1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasaMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
        }
        else if (parteInteiraString.length < qtdCasaMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = '';
        }
        c++;
    }
    stringBuffer.push(parteInteiraString);

    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0');
    return `${parteInteira >= 0 ? 'R$' : '-R$'} ${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;
}

console.log(imprimirBRL(-45));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-45));
