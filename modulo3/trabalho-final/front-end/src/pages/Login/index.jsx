import React, { Component } from 'react'
import axios from 'axios'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            senha: ''
        }
        this.trocaValorState = this.trocaValorState.bind(this)
    }
    trocaValorState(e) {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    logar(e) {
        e.preventDefault()
        const { email, senha } = this.state
        if (email && senha) {
            axios.post('http://localhost:1337/login', {
                email: email,
                senha: senha
            }).then(resp => {
                localStorage.setItem('Authorization', resp.data.token)
                this.props.history.push('/')
            }
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                <section className='section-login'>
                    <div className='div-login'>
                        <h2>Logar</h2>
                        <input type="text" name="email" id="email" placeholder="email" onChange={this.trocaValorState} />
                        <input type="password" name="senha" id="senha" placeholder="Digite a senha" onChange={this.trocaValorState} />
                        <button onClick={this.logar.bind(this)}>Logar</button>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}