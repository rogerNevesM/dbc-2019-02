import React, { Component, Fragment } from 'react'
import Axios from 'axios'
import NavBar from '../../components/NavBar'
import Itens from '../../components/Itens'
import Dados from '../../components/Dados'


class Clientes extends Component {
    constructor(props) {
        super(props)
        this.id = props.match.params.id || null
        this.state = {
            carregando: true,
            clientes: []
        }
    }
    componentWillMount() {
        Axios.get('http://localhost:1337/clientes', {
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
            .then(resp => {
                this.setState({
                    carregando: false,
                    clientes: resp.data.clientes
                })
            })
        if (this.id) {
            Axios.get('http://localhost:1337/cliente/' + (this.id - 1), {
                headers: {
                    authorization: localStorage.getItem('Authorization')
                }
            })
                .then(resp => {
                    this.setState({
                        cliente: resp.data.cliente
                    })
                })
        }
    }
    fechar() {
        this.props.history.push(`/clientes`)
    }
    render() {
        const { clientes, cliente } = this.state
        return (
            <Fragment>
                <NavBar history={this.props.history} />
                <section>
                    <div className='container'>
                        <h1>Clientes</h1>
                        <Itens itens={clientes} path='/clientes' />
                    </div>
                    {cliente && <Dados obj={cliente} fechar={this.fechar.bind(this)} />}
                </section>
            </Fragment>
        )
    }
}

export default Clientes