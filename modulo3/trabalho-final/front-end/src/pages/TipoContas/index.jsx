import React, { Component, Fragment } from 'react'
import Axios from 'axios'
import NavBar from '../../components/NavBar'
import Itens from '../../components/Itens'
import Dados from '../../components/Dados'



class TipoContas extends Component {
    constructor(props) {
        super(props)
        this.id = props.match.params.id || null
        this.state = {
            carregando: true,
            tipoContas: []
        }
    }
    componentWillMount() {
        Axios.get('http://localhost:1337/tipoContas', {
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
            .then(resp => {
                this.setState({
                    carregando: false,
                    tipoContas: resp.data.tipos
                })
            })

        if (this.id) {
            Axios.get('http://localhost:1337/tiposConta/' + (this.id - 1), {
                headers: {
                    authorization: localStorage.getItem('Authorization')
                }
            })
                .then(resp => {
                    this.setState({
                        tipo: resp.data.tipos
                    })
                })
        }
    }
    fechar() {
        this.props.history.push(`/tipoContas`)
    }
    render() {
        const { tipoContas, tipo } = this.state
        return (
            <Fragment>
                <NavBar history={this.props.history} />
                <section>
                    <div className='container'>
                        <h1>Tipo de contas</h1>
                        <Itens itens={tipoContas} path='/tipoContas'/>
                    </div>
                    {tipo && <Dados obj={tipo} fechar={this.fechar.bind(this)} />}

                </section>
            </Fragment>
        )
    }
}

export default TipoContas