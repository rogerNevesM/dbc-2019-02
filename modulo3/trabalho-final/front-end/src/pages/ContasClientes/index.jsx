import React, { Component, Fragment } from 'react'
import Axios from 'axios'
import NavBar from '../../components/NavBar'
import Itens from '../../components/Itens'
import Dados from '../../components/Dados'



class ContasClientes extends Component {
    constructor(props) {
        super(props)
        this.id = props.match.params.id || null
        this.state = {
            carregando: true,
            contasClientes: []
        }
    }
    componentWillMount() {
        Axios.get('http://localhost:1337/conta/clientes', {
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
            .then(resp => {
                this.setState({
                    carregando: false,
                    contasClientes: resp.data.cliente_x_conta
                })
            })
        if (this.id) {
            Axios.get('http://localhost:1337/conta/cliente/' + (this.id - 1), {
                headers: {
                    authorization: localStorage.getItem('Authorization')
                }
            })
                .then(resp => {
                    this.setState({
                        conta: resp.data.conta
                    })
                })
        }
    }
    fechar() {
        this.props.history.push(`/contasClientes`)
    }
    render() {
        const { contasClientes, conta } = this.state
        return (
            <Fragment>
                <NavBar history={this.props.history} />
                <section>
                    <div className='container'>
                        <h1>Tipo de contas</h1>
                        <Itens itens={contasClientes} path='/contasClientes' />
                    </div>
                    {conta && <Dados obj={conta} fechar={this.fechar.bind(this)} />}
                </section>
            </Fragment>
        )
    }
}

export default ContasClientes