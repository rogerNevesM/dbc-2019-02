import React, { Component, Fragment } from 'react'
import Axios from 'axios'
import NavBar from '../../components/NavBar'
import Itens from '../../components/Itens'
import Dados from '../../components/Dados'

class Agencias extends Component {
    constructor(props) {
        super(props)
        this.id = props.match.params.id || null
        this.state = {
            carregando: true,
            agencias: []
        }
    }
    fechar(){
        this.props.history.push(`/agencias`)
    }
    componentWillMount() {
        Axios.get('http://localhost:1337/agencias', {
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
            .then(resp => {
                this.setState({
                    carregando: false,
                    agencias: resp.data.agencias
                })
            })
        if (this.id) {
            Axios.get('http://localhost:1337/agencia/' + (this.id - 1), {
                headers: {
                    authorization: localStorage.getItem('Authorization')
                }
            })
                .then(resp => {
                    this.setState({
                        carregando: false,
                        agencia: resp.data.agencias
                    })
                })
        }
    }
    digital(evt){
        const { agencias } = this.state
        const id =evt.target.parentElement.parentElement.getAttribute('item')
        if(evt.target.checked){ 
            for (let index = 0; index < agencias.length; index++) {// eslint-disable-next-line
                if(agencias[index].id == id)
                agencias[index].is_digital = true
            }
        }
        else{
            for (let index = 0; index < agencias.length; index++) { // eslint-disable-next-line
                if(agencias[index].id == id)
                agencias[index].is_digital = false
            }
        }
        this.setState({
            agencias
        })
    }
    render() {
        const { agencias, agencia } = this.state
        return (
            <Fragment>
                <NavBar history={this.props.history}/>
                <section>
                    <div className='container'>
                        <h1>Agencias</h1>
                        <Itens itens={agencias} path='/agencias' digital={this.digital.bind(this)}/>
                    </div>
                    {agencia && <Dados obj={agencia} fechar={this.fechar.bind(this)}/>}
                </section>
            </Fragment>
        )
    }
}

export default Agencias