import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import Login from './pages/Login'
import Agencias from './pages/Agencias'
import Clientes from './pages/Clientes'
import TipoContas from './pages/TipoContas'
import ContasClientes from './pages/ContasClientes'

import PrivateRoute from './components/PrivateRoute'

import './css/general.css'
import './css/nav.css'
import './css/itens.css'
import './css/dados.css'
import './css/login.css'

function App() {
  return (
    <Router>
      <Route path='/login' component={Login} />
      <PrivateRoute path='/' exact component={Agencias} />
      <PrivateRoute path='/agencias' exact component={Agencias} />
      <PrivateRoute path='/agencias/:id' component={Agencias} />
      <PrivateRoute path='/clientes' exact component={Clientes} />
      <PrivateRoute path='/clientes/:id' component={Clientes} />
      <PrivateRoute path='/tipoContas' exact component={TipoContas} />
      <PrivateRoute path='/tipoContas/:id' component={TipoContas} />
      <PrivateRoute path='/contasClientes' exact component={ContasClientes} />
      <PrivateRoute path='/contasClientes/:id' component={ContasClientes} />
    </Router>

  );
}

export default App;
