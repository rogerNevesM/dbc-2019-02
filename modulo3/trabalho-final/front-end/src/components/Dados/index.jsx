import React, { Component, Fragment } from 'react'



class Dados extends Component {
    constructor(props) {
        super(props)
        this.fechar = props.fechar
    }

    dados(obj) {

        return (
            <Fragment>
                {Object.keys(obj).map((key, index) => {
                    if (typeof obj[key] === 'object') {
                        return (
                            <Fragment>
                                <p>{`${key}:`}</p>
                                <div className='div-margin'>
                                    {this.dados(obj[key])}
                                </div>
                            </Fragment>)
                    } else {
                        return (<p key={index}>{`${key}: ${obj[key]}`}</p>)
                    }
                }
                )
                }
            </Fragment>
        )
    }

    render() {
        const { obj } = this.props
        return (
            <Fragment>
                <div className='div-fechar' onClick={this.fechar}>
                </div>
                <div className='div-dados'>
                    <button className='button-fechar' onClick={this.fechar}>X</button>
                    {this.dados(obj)}
                </div>

            </Fragment>
        )
    }
}

export default Dados