import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'




class Itens extends Component {
    constructor(props) {
        super(props)
        this.digital = props.digital
        this.state = {
            itens: props.itens,
            mostrar: props.itens,
            path: props.path

        }
    }
    componentDidUpdate(prevProps) {
        if (prevProps.itens.length < this.props.itens.length) {
            const { itens } = this.props
            this.setState({
                itens,
                mostrar: itens
            })
        }
    }

    filtrarDigital(evt) {
        if (evt.target.checked) {
            const { itens } = this.state
            const mostrar = itens.filter(item => {
                if (item.is_digital)
                    return true
                return false
            })
            this.setState({
                mostrar
            })
        }
        else {
            const { itens } = this.state
            this.setState({
                mostrar: itens
            })
        }
    }

    buscar(evt) {
        const valor = evt.target.value
        if (valor.length < 1) {
            const { itens } = this.state
            this.setState({
                mostrar: itens
            })
        }
        else {
            const { itens } = this.state
            const mostrar = itens.filter(item => {
                if (item.nome) {// eslint-disable-next-line
                    return item.nome.toLowerCase().includes(valor.toLowerCase()) || item.id == valor
                } else if (item.cliente.nome && item.tipo.nome) { // eslint-disable-next-line
                    return item.cliente.nome.toLowerCase().includes(valor.toLowerCase()) || item.tipo.nome.toLowerCase().includes(valor.toLowerCase()) || item.id == valor
                }
                return false
            })
            this.setState({
                mostrar
            })
        }
    }

    render() {
        const { mostrar, path } = this.state
        return (
            <Fragment>
                <div className='div-filtros'>
                    <input type="text" name="" id="" onChange={this.buscar.bind(this)} placeholder="busque por nome ou id" />
                    {// eslint-disable-next-line
                        path == '/agencias' ? (
                            <Fragment>
                                <p><input type="checkbox" onClick={this.filtrarDigital.bind(this)} />
                                    Agencias Digitais</p>
                            </Fragment>
                        ) : null}
                </div>
                <div className='div-itens'>
                    {mostrar.map((item, index) => (
                        <div key={index} item={item.id} className='item' >
                            <Link to={`${path}/${item.id}`} >


                                {
                                    item.nome ?
                                        (
                                            <h2>{`${item.id}: ${item.nome}`}</h2>
                                        )
                                        : (
                                            <Fragment>
                                                <h2>{`${item.id}: ${item.cliente.nome}`}</h2>
                                                <p>{item.tipo.nome}</p>
                                            </Fragment>
                                        )
                                }

                            </Link>{// eslint-disable-next-line
                            }{path == '/agencias' ?
                                (<Fragment>
                                    {item.is_digital ? (
                                        <p>
                                            <input type="checkbox" onClick={this.digital} checked />
                                            Digital
                                        </p>)
                                        : (<p>
                                            <input type="checkbox" onClick={this.digital} />
                                            Digital</p>)
                                    }

                                </Fragment>
                                ) : null}
                        </div>
                    ))}
                </div>
            </Fragment>
        )
    }
}

export default Itens