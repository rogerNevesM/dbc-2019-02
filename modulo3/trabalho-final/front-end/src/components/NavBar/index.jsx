import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class NavBar extends Component{
    constructor(props){
        super(props)
        this.history = props.history
    }

    logout(){
        localStorage.removeItem('Authorization')
        this.history.push('/login')
    }
    render(){
        return(
            <header>
                <ul>
                    <li><Link to='/agencias'>Agencias</Link></li>
                    <li><Link to='/clientes'>Clientes</Link></li>
                    <li><Link to='/tipoContas'>Tipos de contas</Link></li>
                    <li><Link to='/contasClientes'>Contas de clientes</Link></li>
                    <li onClick={this.logout.bind(this)}>logout</li>
                </ul>
            </header>
        )
    }
}

export default NavBar