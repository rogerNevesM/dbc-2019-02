class PokeApi {// eslint-disable-line no-unused-vars
  constructor() {
    this.a = 'a'
  }

  buscarTodos() {
    this.pokemon = fetch( 'https://pokeapi.co/api/v2/pokemon/' )
    this.pokemon.then( data => data.json() );
  }

  buscarEspecifico( id ) {
    this.pokemon = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` )
    return this.pokemon.then( data => data.json() )
  }
}
