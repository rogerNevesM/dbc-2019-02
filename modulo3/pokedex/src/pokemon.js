class Pokemon {// eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name
    this.id = obj.id
    this.imagem = obj.sprites
    this.altura = obj.height
    this.peso = obj.weight / 10
    this.tipos = obj.types
    this.estatisticas = obj.stats
  }

  conversaoAltura( multiplicador ) {
    return this.altura * multiplicador
  }

  conversaoPeso( divisor ) {
    return this.peso * divisor
  }
}
