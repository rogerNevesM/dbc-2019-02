let pokemonAtual;
function renderizacaoPokemon( pokemon ) {
  pokemonAtual = pokemon
  const nome = document.getElementById( 'nome' )
  const id = document.getElementById( 'id' )
  const imagem = document.getElementById( 'imagem' )
  const altura = document.getElementById( 'altura' )
  const peso = document.getElementById( 'peso' )
  const tipos = document.getElementById( 'tipos' )
  const estatisticas = document.getElementById( 'estatisticas' )
  const card = document.getElementById( 'card' )

  nome.textContent = pokemon.nome
  id.textContent = pokemon.id
  imagem.src = pokemon.imagem.front_default
  altura.textContent = `${ pokemon.conversaoAltura( 10 ) } cm`
  peso.textContent = `${ pokemon.conversaoPeso( 10 ) } kg`

  while ( tipos.hasChildNodes() ) {
    tipos.removeChild( tipos.firstChild )
  }
  pokemon.tipos.forEach( ( tipo ) => {
    const p = document.createElement( 'p' )
    const li = document.createElement( 'li' )
    p.textContent = `${ tipo.type.name };`
    li.appendChild( p )
    tipos.appendChild( li )
  } )

  card.classList.remove( 'bug', 'dark', 'dragon', 'electric', 'fairy', 'fighting', 'fire', 'flying', 'grass', 'ground', 'ice', 'ghost', 'normal', 'poison', 'psychic', 'rock', 'steel', 'water' )
  const [n1] = pokemon.tipos
  card.classList.add( n1.type.name )
  while ( estatisticas.hasChildNodes() ) {
    estatisticas.removeChild( estatisticas.firstChild )
  }
  pokemon.estatisticas.forEach( ( estatistica ) => {
    const p = document.createElement( 'p' )
    const li = document.createElement( 'li' )
    p.textContent = `${ estatistica.stat.name } base: ${ estatistica.base_stat }% `
    li.appendChild( p )
    estatisticas.appendChild( li )
  } )
}

async function buscarPokemon( idPokemon ) {
  const pokeApi = new PokeApi()// eslint-disable-line no-undef
  const pokemonRecebido = await pokeApi.buscarEspecifico( idPokemon )
  const poke = new Pokemon( pokemonRecebido )// eslint-disable-line no-undef
  renderizacaoPokemon( poke )
}

function pokemonAleatorio() {
  let idAleatorio
  let i = 0
  do {
    i += 1
    idAleatorio = Math.floor( Math.random() * 802 ) + 1
    if ( i > 100 ) {
      localStorage.clear()
    }
  } while ( localStorage.getItem( idAleatorio ) )
  localStorage.setItem( idAleatorio, idAleatorio )
  buscarPokemon( idAleatorio )
}

function girarFoto() {
  const imagem = document.getElementById( 'imagem' )
  const { src } = imagem
  if ( src.includes( 'back' ) ) {
    imagem.src = pokemonAtual.imagem.front_default
  } else if ( pokemonAtual.imagem.back_default ) {
    imagem.src = pokemonAtual.imagem.back_default
  }
}

function addEventos() {
  buscarPokemon( 1 )
  const inputReceber = document.getElementById( 'receberId' )
  inputReceber.addEventListener( 'blur', () => {
    const inputRecebe = document.getElementById( 'receberId' )
    const idPokemon = inputRecebe.value
    inputReceber.value = ''
    if ( !idPokemon || idPokemon === pokemonAtual.id ) {
      return
    }
    buscarPokemon( idPokemon )
  } )

  const button = document.getElementById( 'random' )
  button.addEventListener( 'click', pokemonAleatorio )
  window.addEventListener( 'keydown', ( event ) => {
    const testeDiminuirSetas = ( event.keyCode === 40 || event.keyCode === 37 )
    const testeDiminuirLetras = ( event.keyCode === 65 || event.keyCode === 83 )
    if ( ( testeDiminuirSetas || testeDiminuirLetras ) && pokemonAtual.id > 1 ) {
      buscarPokemon( pokemonAtual.id - 1 )
    }
    const testeAumentarSetas = ( event.keyCode === 39 || event.keyCode === 38 )
    const testeAumentarLetras = ( event.keyCode === 68 || event.keyCode === 87 )
    if ( ( testeAumentarSetas || testeAumentarLetras ) && pokemonAtual.id < 802 ) {
      buscarPokemon( pokemonAtual.id + 1 )
    }
  } );

  setInterval( girarFoto, 1000 );
}
window.addEventListener( 'load', addEventos )
