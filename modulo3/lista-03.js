function cardapioIFood(veggie = true, comLactose = true) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ]


    if (comLactose) {
        cardapio.push('pastel de queijo')
    }

    cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa'];
    
     /* cardapio = cardapio.concat([
        'pastel de carne',
        'empada de legumes marabijosa'
    ])  */
    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
        cardapio.splice(cardapio.indexOf('pastel de carne'), 1)
    }
    
    let resultado = cardapio
                            //.filter(alimento => alimento==='cuca de uva') 
                            .map(alimento =>alimento.toLocaleUpperCase());
    console.log(resultado);
    return resultado;
    /* let resultadoFinal = [];
    let i = 0;
    while (i < cardapio.length) {
        resultadoFinal.push(cardapio[i].toUpperCase())
        i++
    }
    return resultadoFinal;*/
} 

//console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

function criarSanduiche(pao, recheio, queijo){
    //console.log(`Seu sanduiche tem o pão ${ pao } com recheio de ${ recheio } e queijo ${ queijo }`)
}

const ingredientes = ['3 queijos', 'frango', 'cheddar'];
criarSanduiche(...ingredientes);

function receberValoreIndefinidos(...valores) {
    //valores.map(valor=>console.log(valor));
}

receberValoreIndefinidos([1,2,3,4,5])

//console.log(... 'marcos');

/* let inputTeste = document.getElementById('campoTeste');

inputTeste.addEventListener('blur', function(){
    console.log("chegou aqui");
}) */

String.prototype.correr = function(upper = false){
    let texto = "estou correndo";
    return upper? texto.toUpperCase() : texto;
}

console.log("ola".correr(true))