import React, { Component } from 'react';
import Home from './components/Home'
import Login from './components/login'
import JsFlix from './components/JsFlix/'
import JsMirror from './components/JsMirror'

import PriveteRoute from './components/PrivateRoute'

import { BrowserRouter as Router, Route } from 'react-router-dom'


export default class App extends Component {
  render() {

    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section>
              <PriveteRoute path="/" exact component={Home}/>
              <Route path="/login"  component={Login}/> 
              <PriveteRoute path="/JsFlix"  component={JsFlix}/>
              <PriveteRoute path="/JsMirror" component={JsMirror}/>
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}
