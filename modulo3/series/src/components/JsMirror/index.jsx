import React, { Component } from 'react';
import axios from 'axios'
import './css/index.css'
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import ListaDeAvaliacoes from './components/ListaDeAvalicoes';
import Nota from './components/Nota'

class App extends Component {
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    const mensagem = ""
    this.refNota = React.createRef();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      nota: 0,
      mensagem,
      listaDeAvaliacoes: false
    }
  }

  componentDidMount() {
    axios.get('https://pokeapi.co/api/v2/pokemon/1/').then(response => console.log(response))
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio
    })
  }



  darNota() {
    const { episodio } = this.state
    this.listaEpisodios.darNota(episodio, this.refNota.current.value)
    const mensagem = `Nota registrada com sucesso!`
    this.setState({
      episodio,
      mensagem
    })
  }

  removerMensagem() {
    const mensagem = ''
    this.setState({
      mensagem
    })
  }

  logout(e) {
    e.preventDefault()
    localStorage.removeItem('Authorization')
    this.props.history.push('/')
  }

  lista(){
    const { listaDeAvaliacoes } = this.state
    this.setState({
      listaDeAvaliacoes: !listaDeAvaliacoes
    })
  }


  render() {
    const { episodio, listaDeAvaliacoes } = this.state
    return (
      <div className="jsmirror">
        <div className="black-mirror">
          {listaDeAvaliacoes ?
            (<ListaDeAvaliacoes lista={this.listaEpisodios}></ListaDeAvaliacoes>)
            : (
              <React.Fragment>
                <EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear.bind(this)} marcarNoComp={this.marcarComoAssistido} />
                <Nota mensagem={this.state.mensagem} refNota={this.refNota} darNota={this.darNota.bind(this)} episodio={episodio} removerMensagem={this.removerMensagem.bind(this)} textoSpan='qual sua nota para este episodio?' />
              </React.Fragment>
            )}
          <button onClick={this.lista.bind(this)}>listaDeAvaliacoes</button>
          <button onClick={this.logout.bind(this)}>logout</button>
        </div>
      </div>
    );

  }

}


export default App;
