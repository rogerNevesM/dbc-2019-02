import React from 'react'

export default class MensagemFlash extends React.Component {
    constructor(props) {
        super(props)
        this.removerMensagem = props.removerMensagem
        this.state = {
            classeMensagem: 'invisible',
            corMensagem: 'verde',
            tempo: 3000
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.mensagem !== prevProps.mensagem && this.props.mensagem !== '') {
            setTimeout(this.apagarMesagem.bind(this), this.state.tempo)
            this.setState({
                classeMensagem: 'visible'
            })
        }
        if (this.props.cor !== prevProps.cor) {
            this.setState({
                corMensagem: this.props.cor
            })
        }
        if (this.props.tempo !== prevProps.tempo) {
            this.setState({
                tempo: this.props.tempo
            })
        }
    }

    apagarMesagem() {
        const classeMensagem = 'invisible'
        this.setState({
            classeMensagem
        })
        setTimeout(this.removerMensagem, 2000)
    }

    render() {
        return (
            <p className={`${this.state.corMensagem} ${this.state.classeMensagem}`}>{this.props.mensagem}</p>
        )
    }
}