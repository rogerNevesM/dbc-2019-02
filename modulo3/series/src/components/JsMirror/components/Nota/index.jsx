import React from 'react'
import MensagemFlash from './components/MensagemFlash'
import './css/mensagem.css'

class Mensagem extends React.Component {
    constructor(props) {
        super(props)
        this.darNota = props.darNota
        this.removerMensagem = props.removerMensagem
        this.refNota = props.refNota
        this.state = {
            corMensagem: 'verde',
            tempoMensagem: 3000,
            classeMensagem: 'invisible',
        }
    }

    mudarCor(evt) {
        const corMensagem = evt.target.value
        this.setState({
            corMensagem
        })
    }
    mudarTempo(evt) {
        const tempoMensagem = evt.target.value>0?evt.target.value*1000:3000
        this.setState({
            tempoMensagem
        })
    }

    render() {
        return (
            <React.Fragment>
                <div className="nota">{this.props.episodio.qtdVezesAssistido && (
                    <div className="nota">
                        <div className="dar-nota">
                            {this.props.textoSpan &&(<span>{this.props.textoSpan}</span>)}
                            <p>1</p><input type="range" min="1" max="5" value={this.props.episodio.nota} ref={this.refNota} onChange={this.darNota} /><p>5</p>
                        </div>
                        <MensagemFlash mensagem={this.props.mensagem} cor={this.state.corMensagem} tempo={this.state.tempoMensagem} removerMensagem={this.removerMensagem.bind(this)}/>
                        <select onChange={this.mudarCor.bind(this)} value={this.state.corMensagem}>
                            <option value="verde">verde</option>
                            <option value="vermelho">vermelho</option>
                        </select>
                        <input type="number" placeholder="tempo da mensagem" onChange={this.mudarTempo.bind(this)}/>
                    </div>
                )}</div>
            </React.Fragment>
        )
    }

}

export default Mensagem