import React, {Component} from 'react'
import './css/button.css'

class Button extends Component{
    constructor(props){
        super(props)
        this.click = props.click
        this.texto = props.texto
    }
    render(){
        return(
            <button onClick={this.click} className='info-button'> { this.texto } </button>
        )
    }
}

export default Button