import React from 'react'
import Button from './components/Button'
import './css/episodio-padrao.css'

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            <div className="info">
                <Button click={ props.marcarNoComp.bind(this)} texto='Já assisti' /> 
                <Button click={ props.sortearNoComp.bind(this)} texto='proximo'/>  
            </div>
            <h2> {episodio.nome} </h2>
            <img src={episodio.thumbUrl} alt={episodio.nome} />
            <h4>Ja assisti? {episodio.assistido ? 'Sim' : 'Não'}{episodio.qtdVezesAssistido ? `, ${episodio.qtdVezesAssistido} x` : ''} </h4>
            <div className="info">
                <p>{episodio.duracaoEmMin} </p>
                <p> Temp/Ep: {episodio.temporadaEpisodio}</p>
            </div>
            <p>{episodio.nota || "Sem nota"}</p>
        </React.Fragment>
    )
}

export default EpisodioPadrao