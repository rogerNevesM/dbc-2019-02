import React, { Component } from 'react'
import ListaEpisodios from '../../models/ListaEpisodios'

class ListaDeAvaliacao extends Component {
    constructor(props) {
        super(props)
        this.lista = props.lista ? props.lista : new ListaEpisodios()
    }
    render() {
        return (
            <React.Fragment>
                <table>
                <thead>
                    <tr>
                        <th>episodio</th>
                        <th>nota</th>
                        <th>Temporada</th>
                        <th>episodio</th>
                    </tr>
                </thead>
                <tbody>
                    {this.lista.organizarExibicao().map(( ep, key ) => ep.nota? (
                                <React.Fragment key={key}>
                                    <tr>
                                        <td>{ep.nome}</td>
                                        <td>{ep.nota}</td>
                                        <td>{ep.temporada}</td>
                                        <td>{ep.ordemEpisodio}</td>
                                    </tr>
                                </React.Fragment>
                            ):null)
                    }
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default ListaDeAvaliacao