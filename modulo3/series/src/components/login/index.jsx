import React, {Component} from 'react'
import * as axios from 'axios'

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            email: '',
            password: ''
        }
        this.trocaValorState = this.trocaValorState.bind( this )
    }
    trocaValorState(e){
        const {name , value} = e.target
        this.setState({
            [name] : value
        })
    }
    logar(e){
        e.preventDefault()
        const { email , password } = this.state
        if (email && password ){
            //executo a regra de login
            axios.post('http://localhost:1337/login',{
                email: email,
                password: password
            }).then( resp =>{
                localStorage.setItem('Authorization',resp.data.token)
                this.props.history.push('/')
            }
            )
        }
    }

    render(){
        return(
            <React.Fragment>
                <h5>Logar</h5>
                <input type="text" name="email" id="email" placeholder="email" onChange={ this.trocaValorState }/>
                <input type="password" name="password" id="password" placeholder="Digite o password" onChange={ this.trocaValorState }/>
                <button onClick={ this.logar.bind( this ) }>Logar</button>
            </React.Fragment>
        )
    }
}