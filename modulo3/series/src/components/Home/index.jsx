import React from 'react'
import { Link } from 'react-router-dom'
import './css/home.css'


const Inicial = props => {
    return (
        <React.Fragment>
            <div className="home">
                <h1>Escolha um projeto</h1>
                <div className="escolha">
                    <Link className="button button1" to="/JsFlix">JsFlix</Link>
                    <Link className="button button2" to="JsMirror">jsMirror</Link>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Inicial