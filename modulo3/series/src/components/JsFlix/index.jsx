import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import ListaSeries from './models/ListaSeries'
import ListarSeries from './components/ListarSeries'
import Creditos from './components/Creditos'
import Header from './components/Header'
import './css/general.css'



export default class App extends Component {
    constructor(props) {
        super(props)
        this.lista = new ListaSeries();
        this.state = {
            series: this.lista.series,
        }
    }

    pesquisarNome = (evt) => {
        const series = this.lista.queroTitulo(evt.target.value)
        this.setState({
            series
        })
    }

    render() {
        const { series } = this.state
        return (
            <div className="Jsflix">
                <Header pesquisarNome={this.pesquisarNome.bind()} />
                <Router>
                    <Route path='/' render={() => <ListarSeries series={series} />} ></Route>
                    <Route path='/creditos/:id' render={(props) => <Creditos {...props} lista={this.lista} />} ></Route>
                </Router>
            </div>
        );
    }
}