import PropTypes from 'prop-types'
import Serie from './Serie';

export default class ListaSeries {
    constructor() {
        this.series = [
        { "titulo": "Stranger Things", "anoEstreia": 2016, "diretor": ["Matt Duffer", "Ross Duffer"], "genero": ["Suspense", "Ficcao Cientifica", "Drama"], "elenco": ["Winona Ryder", "David Harbour", "Finn Wolfhard", "Millie Bobby Brown", "Gaten Matarazzo", "Caleb McLaughlin", "Natalia Dyer", "Charlie Heaton", "Cara Buono", "Matthew Modine", "Noah Schnapp"], "temporadas": 2, "numeroEpisodios": 17, "distribuidora": "Netflix" },
        { "titulo": "Game Of Thrones", "anoEstreia": 2011, "diretor": ["David Benioff", "D. B. Weiss", "Carolyn Strauss", "Frank Doelger", "Bernadette Caulfield", "George R. R. Martin"], "genero": ["Fantasia", "Drama"], "elenco": ["Peter Dinklage", "Nikolaj Coster-Waldau", "Lena Headey", "Emilia Clarke", "Kit Harington", "Aidan Gillen", "Iain Glen ", "Sophie Turner", "Maisie Williams", "Alfie Allen", "Isaac Hempstead Wright"], "temporadas": 7, "numeroEpisodios": 67, "distribuidora": "HBO" },
        { "titulo": "The Walking Dead", "anoEstreia": 2010, "diretor": ["Jolly Dale", "Caleb Womble", "Paul Gadd", "Heather Bellson"], "genero": ["Terror", "Suspense", "Apocalipse Zumbi"], "elenco": ["Andrew Lincoln", "Jon Bernthal", "Sarah Wayne Callies", "Laurie Holden", "Jeffrey DeMunn", "Steven Yeun", "Chandler Riggs ", "Norman Reedus", "Lauren Cohan", "Danai Gurira", "Michael Rooker ", "David Morrissey"], "temporadas": 9, "numeroEpisodios": 122, "distribuidora": "AMC" },
        { "titulo": "Band of Brothers", "anoEstreia": 20001, "diretor": ["Steven Spielberg", "Tom Hanks", "Preston Smith", "Erik Jendresen", "Stephen E. Ambrose"], "genero": ["Guerra"], "elenco": ["Damian Lewis", "Donnie Wahlberg", "Ron Livingston", "Matthew Settle", "Neal McDonough"], "temporadas": 1, "numeroEpisodios": 10, "distribuidora": "HBO" },
        { "titulo": "The JS Mirror", "anoEstreia": 2017, "diretor": ["Lisandro", "Jaime", "Edgar"], "genero": ["Terror", "Caos", "JavaScript"], "elenco": ["Daniela Amaral da Rosa", "Antônio Affonso Vidal Pereira da Rosa", "Gustavo Lodi Vidaletti", "Bruno Artêmio Johann Dos Santos", "Márlon Silva da Silva", "Izabella Balconi de Moura", "Diovane Mendes Mattos", "Luciano Maciel Figueiró", "Igor Ceriotti Zilio", "Alexandra Peres", "Vitor Emanuel da Silva Rodrigues", "Raphael Luiz Lacerda", "Guilherme Flores Borges", "Ronaldo José Guastalli", "Vinícius Marques Pulgatti"], "temporadas": 1, "numeroEpisodios": 40, "distribuidora": "DBC" },
        { "titulo": "10 Days Why", "anoEstreia": 2010, "diretor": ["Brendan Eich"], "genero": ["Caos", "JavaScript"], "elenco": ["Brendan Eich", "Bernardo Bosak"], "temporadas": 10, "numeroEpisodios": 10, "distribuidora": "JS" },
        { "titulo": "Mr. Robot", "anoEstreia": 2018, "diretor": ["Sam Esmail"], "genero": ["Drama", "Techno Thriller", "Psychological Thriller"], "elenco": ["Rami Malek", "Carly Chaikin", "Portia Doubleday", "Martin Wallström", "Christian Slater"], "temporadas": 3, "numeroEpisodios": 32, "distribuidora": "USA Network" },
        { "titulo": "Narcos", "anoEstreia": 2015, "diretor": ["Paul Eckstein", "Mariano Carranco", "Tim King", "Lorenzo O Brien"], "genero": ["Documentario", "Crime", "Drama"], "elenco": ["Wagner Moura", "Boyd Holbrook", "Pedro Pascal", "Joann Christie", "Mauricie Compte", "André Mattos", "Roberto Urbina", "Diego Cataño", "Jorge A. Jiménez", "Paulina Gaitán", "Paulina Garcia"], "temporadas": 3, "numeroEpisodios": 30, "distribuidora": null },
        { "titulo": "Westworld", "anoEstreia": 2016, "diretor": ["Athena Wickham"], "genero": ["Ficcao Cientifica", "Drama", "Thriller", "Acao", "Aventura", "Faroeste"], "elenco": ["Anthony I. Hopkins", "Thandie N. Newton", "Jeffrey S. Wright", "James T. Marsden", "Ben I. Barnes", "Ingrid N. Bolso Berdal", "Clifton T. Collins Jr.", "Luke O. Hemsworth"], "temporadas": 2, "numeroEpisodios": 20, "distribuidora": "HBO" },
        { "titulo": "Breaking Bad", "anoEstreia": 2008, "diretor": ["Vince Gilligan", "Michelle MacLaren", "Adam Bernstein", "Colin Bucksey", "Michael Slovis", "Peter Gould"], "genero": ["Acao", "Suspense", "Drama", "Crime", "Humor Negro"], "elenco": ["Bryan Cranston", "Anna Gunn", "Aaron Paul", "Dean Norris", "Betsy Brandt", "RJ Mitte"], "temporadas": 5, "numeroEpisodios": 62, "distribuidora": "AMC" }
    ].map(e => new Serie( e.titulo, e.anoEstreia, e.diretor, e.genero, e.elenco, e.temporadas, e.numeroEpisodios, e.distribuidora ) )

    }

    invalidas() {
        let ano = new Date()
        ano = ano.getFullYear()// eslint-disable-next-line
        let str = this.series.map(serie => {
            let testeNull = false
            Object.keys(serie).forEach(function (item) {
                if (serie[item] == null)
                    testeNull = true
            });
            if ((serie.anoEstreia > ano || testeNull))
                return ` ${serie.titulo} -`
        })
        str.unshift('Series invalidas:')
        str = str.join('')
        return str.substr(0, str.length - 2)
    }

    filtrarPorAno(ano) {
        let array = this.series.map(serie => serie.anoEstreia >= ano ? serie : null)
        array = array.filter(serie => serie != null)
        return array
    }

    procurarPorNome(nome) {
        const verifica = this.series.map(serie => serie.elenco.toString().includes(nome) ? true : false)
        return verifica.includes(true)
    }

    mediaDeEpisodios() {
        let somaEp = 0
        let qtdSerie = this.series.length
        this.series.forEach(serie => somaEp += serie.numeroEpisodios)
        return somaEp / qtdSerie
    }

    totalSalarios(indice) {
        const serieSalario = this.series[indice]
        const qtdDiretores = serieSalario.diretor.length
        const qtdElenco = serieSalario.elenco.length
        const salarioDiretores = qtdDiretores * 100000
        const salarioElenco = qtdElenco * 40000
        return salarioDiretores + salarioElenco
    }

    queroGenero(genero) {
        let array = this.series.map(serie => serie.genero.includes(genero) ? serie : null)
        array = array.filter(serie => serie != null)
        return array
    }

    queroTitulo(nome) {
        let array = this.series.map(serie => serie.titulo.toLowerCase().includes(nome.toLowerCase()) ? serie : null)
        array = array.filter(serie => serie != null)
        return array
    }
    creditos(indice){
        const titulo = this.series[indice].titulo;
        let diretores = this.series[indice].diretor.map(diretor => diretor.split(' ').reverse().join(' ') )
        diretores.sort()
        diretores = diretores.map(diretor => diretor.split(' ').reverse().join(' ') )
        let elenco = this.series[indice].elenco.map(elenco => elenco.split(' ').reverse().join(' ') )
        elenco.sort()
        elenco = elenco.map(elenco => elenco.split(' ').reverse().join(' ') )
        return { titulo, diretores, elenco }
    }

    hashtag() {
        let resultado;
        this.series.forEach(function (serie) {
            let verifica = serie.elenco.map(e => abreviacao(e))
            if (!verifica.includes(false)) {
                resultado = serie.elenco.map(e => {
                    let index = e.indexOf('.') - 1
                    return e.substr(index, 1)
                })
                resultado.unshift('#')
                
            }
        })
        return resultado.join('')
    }

}
function abreviacao(str) {
        return str.includes('. ')
    }
ListaSeries.PropTypes = {
    invalidas: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    procurarPorNome: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    queroGenero: PropTypes.array,
    queroNome: PropTypes.array,
    hashtag: PropTypes.array,
    abreviacao: PropTypes.array,
}

