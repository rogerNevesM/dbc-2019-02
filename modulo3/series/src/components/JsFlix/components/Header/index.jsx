import React from 'react'
import logo from './img/netflix.png'
import './css/header.css'

const Cabecalho = props => {
    const { pesquisarNome } = props
    return (
        <React.Fragment>
            <header className="header" >
                <div className="generos-hover">Generos</div>
                <input type="search" onChange={pesquisarNome} />
                <img src={logo} alt=""/>
            </header>
        </React.Fragment>
    )
}

export default Cabecalho