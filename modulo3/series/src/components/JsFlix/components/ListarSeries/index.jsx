import React from 'react'
import './css/series.css'

const ListarSeries = props => {
    const { series } = props
    return (
        <React.Fragment>
            <div className="center" >
                <div className="container">
                {series.map((serie,index) => (
                <article className="series" key={index}>
                    <h1>{serie.titulo}</h1>
                    <p>temporada(s): {serie.temporadas}</p>
                </article>))}
                </div>
            </div>
        </React.Fragment>
    )
}

export default ListarSeries