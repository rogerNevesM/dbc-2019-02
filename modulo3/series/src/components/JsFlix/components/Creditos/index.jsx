import React from 'react'

class Creditos extends React.Component {
    constructor( props ) {
        super( props )
        this.id = props.match.params.id
        this.lista = props.lista
        console.log(props)
        this.creditos = this.lista.creditos( this.id )
    }
    render() {
        return (
            <React.Fragment>
                <div >
                    {console.log(123)
                    }
                    <h1>{this.creditos.titulo}</h1>
                    <h2>Diretores</h2>
                    {this.creditos.diretores.map(diretor => (
                        <p>
                            <p>{diretor}</p>
                        </p>))}
                    <h2>Elenco</h2>
                    {this.creditos.elenco.map(ator => (
                        <p>
                            <p>{ator}</p>
                        </p>))}
                </div>
            </React.Fragment>
        )
    }
}

export default Creditos