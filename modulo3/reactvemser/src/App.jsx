import React,{Component} from 'react';
//import logo from './logo.svg';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import TesteRenderizacao from './components/TesteRenderizacao';
/* import Filho from './exemplos/Filhos'; */
//import CompA, { CompB } from './ExemploComponenteBasico';
//import Familia from './exemplos/Familia';

class App extends Component{
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    const mensagens = { nota: ""}
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      mensagens
    }    
  }

  sortear = ()=>{
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido= () =>{
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( { 
      episodio
    } )
  }

  darNota(nota){
    const { episodio } = this.state
    this.listaEpisodios.darNota( episodio, nota )
    const mensagens = { nota: `Nota registrada com sucesso!`}
    this.setState({ 
      episodio,
      mensagens
    })
    setTimeout(this.removerMensagem.bind(this),5000)
  }

  gerarCampoNota(){
    const { episodio, mensagens } = this.state
    return(<div className="nota">{ episodio.qtdVezesAssistido &&(
      <div className="nota">
      <div className="dar-nota">
      <p>1</p><input type="range" min="1" max="5" value={ episodio.nota } onChange={ (e) => this.darNota(e.target.value) }/><p>5</p>
      </div>
      <p className="sucesso">{ mensagens.nota }</p>
      </div>
    )}</div> )
  }

  removerMensagem (){
    const mensagens = { nota: ""}
    this.setState({ 
      mensagens
    })
  }



  render(){
    const { episodio } = this.state
    return (
      <div className="App">
       <div className="app-header">
         <EpisodioPadrao episodio={ episodio } sortearNoComp={this.sortear.bind(this)} marcarNoComp={this.marcarComoAssistido}/>
         { this.gerarCampoNota() }
         <TesteRenderizacao nome="roger">
          <h4>aqui novamente</h4>
         </TesteRenderizacao>
       </div>
      </div>
    );

  }
  
}


export default App;
