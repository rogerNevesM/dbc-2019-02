import React from 'react'

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            <div className="info">
                <button onClick={() => props.marcarNoComp()}> Já assisti </button>
                <button onClick={ props.sortearNoComp /* .bind(this) */}> proximo </button>
            </div>
            <h2> {episodio.nome} </h2>
            <img src={episodio.thumbUrl} alt={episodio.nome} />
            <h4>Ja assisti? {episodio.assistido ? 'Sim' : 'Não'}{episodio.qtdVezesAssistido ? `, ${episodio.qtdVezesAssistido} x` : ''} </h4>
            <div className="info">
                <p>{episodio.duracaoEmMin} </p>
                <p> Temp/Ep: {episodio.temporadaEpisodio}</p>
            </div>
            <p>{episodio.nota || "Sem nota"}</p>
        </React.Fragment>
    )
}

export default EpisodioPadrao