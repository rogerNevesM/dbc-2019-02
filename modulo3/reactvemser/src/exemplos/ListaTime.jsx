function _sortear(min,max) {
    min = Math.ceil(min)
    max = Math.ceil(max)
    return Math.floor(Math.random*(max-min))+min
}

export default class ListaTime {
    constructor() {
        this.todos = [
            {nome:'internacional', estado: 'RS'},
            {nome:'Grêmio', estado: 'RS'},
            {nome:'São José', estado: 'RS'},
            {nome:'Caxias', estado: 'RS'},
            {nome:'Juventude', estado: 'RS'},
        ]
    }

    get timesAleatorios() {
        const indice = _sortear(0, this.todos.length )
        return this.todos[ indice ]
    }
}