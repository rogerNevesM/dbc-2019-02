
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NoturnosPorUltimoTest
{
    @Test
    public void RetornaListaDeElfosComAEstrategiaCorreta(){
        NoturnosPorUltimo ataque = new NoturnosPorUltimo();
        ArrayList<Elfo> atacantes = new ArrayList<>();
        ArrayList<Elfo> organizada = new ArrayList<>();
        ElfoVerde verde1= new ElfoVerde("Verde");
        ElfoVerde verde2= new ElfoVerde("Verde");
        ElfoVerde verde3= new ElfoVerde("Verde");
        ElfoVerde verde4= new ElfoVerde("Verde");
        ElfoNoturno noturno1 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno2 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno3 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno4 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno5 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno6 = new ElfoNoturno("Noturno");
        
        atacantes.add(noturno1);
        atacantes.add(verde1);
        atacantes.add(noturno2);
        atacantes.add(verde2);
        atacantes.add(noturno5);
        atacantes.add(verde3);
        atacantes.add(verde4);
        atacantes.add(noturno3);
        atacantes.add(noturno4);
        atacantes.add(noturno6);
        
        organizada.add(verde4);
        organizada.add(verde3);
        organizada.add(verde2);
        organizada.add(verde1);
        organizada.add(noturno1);
        organizada.add(noturno2);
        organizada.add(noturno5);
        organizada.add(noturno3);
        organizada.add(noturno4);
        organizada.add(noturno6);
        
        atacantes=ataque.getOrdemDeAtaque(atacantes);
        
        assertEquals(organizada,atacantes);
    }
}
