

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;


public class InventarioTest{

    @Test  
    public void criarInventarioSemQuantidadeInformada(){
        Inventario mochila= new Inventario(0);
        assertEquals(0, mochila.getTamanhoMochila());
    }
    
    // @Test  
    // public void criarInventarioComQuantidadeInformada(){
        // Inventario mochila= new Inventario();
        // assertEquals(34, mochila.getTamanhoMochila());
    // }
    
    @Test  
    public void adicionarItem(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);     
        
        assertEquals(espada, mochila.obter(0));
        assertEquals(escudo, mochila.obter(1));
    }
    
    // @Test 
    // public void adicionarDoisItensComEspacoParaUmNaoAdicionaSegundo(){
        // Inventario mochila= new Inventario();
        // Item espada = new Item(2, "Espada");
        // Item escudo = new Item(2, "Escudo");
        // mochila.adicionar(espada);
        // mochila.adicionar(escudo);     
        
        // assertEquals(espada, mochila.obter(0));
        // assertEquals(1, mochila.getTamanhoMochila());
    // }
    
    @Test
    public void removerItem(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        mochila.remove(1);
        assertEquals(espada,mochila.obter(0));
        assertEquals(capacete,mochila.obter(1));
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.remove(1);
        mochila.adicionar(capacete);
        assertEquals(espada,mochila.obter(0));
        assertEquals(capacete,mochila.obter(1));
    }

    @Test
    public void imprimeDescricaoItens(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals("Espada, Escudo, Capacete", mochila.getDescricoesItens());
    }
    
    @Test
    public void imprimeDescricaoItensNenhumItem(){
        Inventario mochila= new Inventario(0);
        assertEquals("", mochila.getDescricoesItens());
    }    
    
    @Test
    public void retornarItemComMaiorQuantidade(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals(escudo, mochila.retornarItemComMaiorQuantidade());
    }
    
    @Test
    public void retornarMaiorQuantidadeInventarioSemItens(){
        Inventario mochila= new Inventario(0);
        assertNull(mochila.retornarItemComMaiorQuantidade());
    }    
    
    @Test
    public void retornarItensComMesmaQuantidade(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(20, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals(escudo, mochila.retornarItemComMaiorQuantidade());
    }   
    
    @Test
    public void verificarDescricaoDoItem(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        assertEquals(flecha, mochila.buscarItem("Flecha"));        
    }
    
    @Test
    public void verificarDescricaoDoItemNaoExiste(){
        Inventario mochila= new Inventario(0);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        assertNull(mochila.buscarItem("Adaga"));        
    }
    
     @Test
    public void verificarDescricaoDoItemInvenrioVazio(){
        Inventario mochila= new Inventario(0);
        assertNull(mochila.buscarItem("Adaga"));        
    }
    
    @Test
    public void inverterItensInventario(){
        Inventario mochila= new Inventario(0);
        ArrayList<Item> inverso = new ArrayList<>();
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        inverso.add(capacete);
        inverso.add(flecha);
        inverso.add(espada);
        assertEquals(inverso, mochila.inverterListaItens());        
    }    
    
    @Test
    public void inverterInventarioVazio(){
        Inventario mochila= new Inventario(0);
        assertTrue(mochila.inverterListaItens().isEmpty());
    }
    
    @Test
    public void ordenarItensASC(){
        Inventario mochila= new Inventario(0);
        ArrayList<Item> ordenado = new ArrayList<>();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(1, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        ordenado.add(capacete);
        ordenado.add(espada);
        ordenado.add(escudo);
        mochila.ordenarItens();
        assertEquals(ordenado,mochila.getItens());
        
    }
    
    @Test
    public void ordenarItensDESC(){
        Inventario mochila= new Inventario(0);
        ArrayList<Item> ordenado = new ArrayList<>();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(1, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        ordenado.add(escudo);
        ordenado.add(espada);
        ordenado.add(capacete);
        mochila.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(ordenado,mochila.getItens());
        
    }
}
