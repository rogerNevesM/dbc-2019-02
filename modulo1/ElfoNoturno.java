public class ElfoNoturno extends Elfo implements Atacar{
    
    public ElfoNoturno(String nome){
        super(nome);
        this.qtdXpPorAtaque = 3;
        this.qtdDanoRecebidoPorAtaque = 15.0;
    }
    
}
