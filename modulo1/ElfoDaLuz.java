import java.util.*;

public class ElfoDaLuz extends Elfo implements Atacar{
    private int ataqueComEspada;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de galvorn"
            )
    );
    
    {
        this.ataqueComEspada = 0;
    }
    public ElfoDaLuz(String nome){
        super(nome);
        this.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public Item getEspada (){
        return this.getInventario().buscarItem(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    public void atacarComEspada(Dwarf dwarf){
        if(getEspada().getQuantidade()>0){
            dwarf.sofrerDano(10.0);
            this.ataqueComEspada ++;
            this.aumentarXp();
            if(ataqueComEspada%2 == 0){
                this.curarVida(10.0);
            }
            else{
                this.sofrerDano(21.0);
            }
        }
    }
    
    public void perderItem(Item item){
        boolean possoPerder= !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
   
}
