

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXpSofrerDanoDwarf(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atacar(novoDwarf);
        assertEquals(3, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(100.0,novoDwarf.getQtdVida(),1e-9);
        assertEquals(85.0,novoElfo.getQtdVida(),1e-9);
    }
    
    @Test
    public void atirar7FlechasEMprre(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.getInventario().buscarItem("Flecha").setQuantidade(200);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        novoElfo.atacar(novoDwarf);
        assertEquals(21, novoElfo.getExperiencia());
        assertEquals(193, novoElfo.getQtdFlecha());
        assertEquals(40.0,novoDwarf.getQtdVida(),1e-9);
        assertEquals(0.0,novoElfo.getQtdVida(),1e-9);
        assertEquals(Status.MORTO,novoElfo.getStatus());
    }
}
