

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    private final double DELTA = 1e-9;
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        assertEquals(110.0,novoDwarf.getQtdVida(),DELTA);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.sofrerDano(10.0);
        assertEquals(100.0,novoDwarf.getQtdVida(),DELTA);
        assertEquals(Status.SOFREU_DANO,novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        for(int i=0;i < 11;i++ ){
            novoDwarf.sofrerDano(10.0);
        }
        assertEquals(0.0,novoDwarf.getQtdVida(),DELTA);
        assertEquals(Status.MORTO,novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfUsaEscudoETomaMetadeDoDano(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.equiparEscudo();
        novoDwarf.sofrerDano(10.0);
        assertEquals(105.0,novoDwarf.getQtdVida(),DELTA);
        assertEquals(Status.SOFREU_DANO,novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfUsaEscudoUmaVezETomaSofreDoisAtaques(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.equiparEscudo();
        novoDwarf.sofrerDano(10.0);
        novoDwarf.sofrerDano(10.0);
        assertEquals(95.0,novoDwarf.getQtdVida(),DELTA);
        assertEquals(Status.SOFREU_DANO,novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfGanhaItem(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        Item machado= new Item(1,"Machado");
        novoDwarf.ganharItem(machado);
        assertEquals(2,novoDwarf.getQtdItens());
    }
    
    @Test
    public void dwarfGanhaEPerdeItem(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        Item machado= new Item(1,"Machado");
        novoDwarf.ganharItem(machado);
        assertEquals(2,novoDwarf.getQtdItens());
        novoDwarf.perderItem(machado);
        assertEquals(1,novoDwarf.getQtdItens());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        assertNotNull(novoDwarf.inventario.buscarItem("Escudo"));
    }
}
