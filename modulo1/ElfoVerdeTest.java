

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXpSofrerDanoDwarf(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atacar(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(100.0,novoDwarf.getQtdVida(),1e-9);
    }
    
    @Test
    public void ganharItensPossiveis(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item espada = new Item(1,"Espada de aço valariano");
        novoElfo.ganharItem(espada);
        assertEquals((new Item(1,"Arco")),novoElfo.getInventario().buscarItem("Arco"));
        assertEquals((new Item(2,"Flecha")),novoElfo.getInventario().buscarItem("Flecha"));
        assertEquals(espada,novoElfo.getInventario().buscarItem("Espada de aço valariano"));
    }
    
     @Test
    public void perderItensPossiveis(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item espada = new Item(1,"Espada de aço valariano");
        novoElfo.ganharItem(espada);
        novoElfo.perderItem(espada);
        assertEquals((new Item(1,"Arco")),novoElfo.getInventario().buscarItem("Arco"));
        assertEquals((new Item(2,"Flecha")),novoElfo.getInventario().buscarItem("Flecha"));
        assertNull(novoElfo.getInventario().buscarItem("Espada de aço valariano"));
    }
    
    @Test
    public void ganharItensNaoPossiveis(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item espada = new Item(1,"Espada larga");
        novoElfo.ganharItem(espada);
        assertEquals((new Item(1,"Arco")),novoElfo.getInventario().buscarItem("Arco"));
        assertEquals((new Item(2,"Flecha")),novoElfo.getInventario().buscarItem("Flecha"));
        assertNull(novoElfo.getInventario().buscarItem("Espada larga"));
    }
    
    @Test
    public void perderItensNaoPossiveis(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arco = novoElfo.getInventario().buscarItem("Arco");
        novoElfo.perderItem(arco);
        assertEquals((new Item(1,"Arco")),novoElfo.getInventario().buscarItem("Arco"));
        assertEquals((new Item(2,"Flecha")),novoElfo.getInventario().buscarItem("Flecha"));
        assertEquals(arco,novoElfo.getInventario().buscarItem("Arco"));
    }
}

