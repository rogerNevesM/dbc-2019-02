import java.util.*;

public class Inventario{
    private ArrayList<Item> mochila;
    
    public Inventario(int quantidade){
        mochila = new ArrayList<>(quantidade);
    }
    
    public ArrayList<Item> getItens(){
        return this.mochila;
    }
    
    public int getTamanhoMochila(){
        return mochila.size();
    }
    
    public void adicionar(Item item){
        this.mochila.add(item);     
    }
    
    public void remover(Item item){
        if(this.mochila.contains(item)){
            this.mochila.remove(item);
        }
    }
    
    public void adicionarVarios(ArrayList<Item> itens){
        this.mochila.addAll(itens);     
    }
    
    public Item obter(int posicao){
        if(posicao>=mochila.size()){
            return null;
        }
        return this.mochila.get(posicao);
    }
    
    public void remove(int posicao){
        this.mochila.remove(posicao);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0 ; i<mochila.size();i++){
            Item item = mochila.get(i);
            if(item!=null ){
                descricoes.append(item.getDescricao());
                descricoes.append(", ");
            }
        }
        return (descricoes.length() > 1 ? 
                descricoes.substring(0,(descricoes.length()-2)) :
                descricoes.toString());
    }
    
    public Item retornarItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        for(int i = 0 ; i < mochila.size(); i++){
            if(mochila.get(i) != null){
                if(mochila.get(i).getQuantidade() > maiorQuantidade){
                    maiorQuantidade = mochila.get(i).getQuantidade();
                    indice = i;
                }
            }
        }
        return this.mochila.size() > 0 ? this.mochila.get(indice) : null;
    }
    
    public Item buscarItem(String descricao){
        for(int i = 0 ; i<mochila.size();i++){
            if(mochila.get(i).getDescricao().equals( descricao)){
                return mochila.get(i);
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverterListaItens(){
        ArrayList<Item> itensInvertidos = new ArrayList<>();
        
        for(int i=this.getTamanhoMochila(); i>0; i--){
            itensInvertidos.add(this.mochila.get(i-1));
        }
        
        // itensInvertidos.addAll(this.mochila);
        // Collections.reverse(itensInvertidos);
        return itensInvertidos;
    }
    
    
    
    public void ordenarItens(){
        ArrayList<Item> itensOrdenados = new ArrayList<>();
        Item item;
        //if(!itensOrdenados.isEmpty()){
            itensOrdenados.add(this.obter(0));
            for(int i = 1; i < this.getTamanhoMochila();i++){
                item = this.obter(i);
                for(int k = 0; k< itensOrdenados.size();k++){
                    if(item.getQuantidade()< itensOrdenados.get(k).getQuantidade()){
                        itensOrdenados.add(k,item);
                        break;
                    }   
                }
                if(!itensOrdenados.contains(item)){
                    itensOrdenados.add(item);
                }
        }
        this.mochila = itensOrdenados;
        //}
        
    }
    
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        ArrayList<Item> itensOrdenados = new ArrayList<>();
        Item item;
        if(ordenacao == TipoOrdenacao.ASC){
            this.ordenarItens();
        }
        else{
            itensOrdenados.add(this.obter(0));
            for(int i = 1; i < this.getTamanhoMochila();i++){
                item = this.obter(i);
                for(int k = 0; k< itensOrdenados.size();k++){
                    if(item.getQuantidade()> itensOrdenados.get(k).getQuantidade()){
                        itensOrdenados.add(k,item);
                        break;
                    }
                }
                if(!itensOrdenados.contains(item)){
                    itensOrdenados.add(item);
                }
            
            }
        }
        this.mochila = itensOrdenados;
    }
}
