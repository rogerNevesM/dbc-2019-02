import java.util.*;
public class NoturnosPorUltimo implements Estrategia
{
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> retorno = new ArrayList<>();

        for(Elfo elfo : atacantes){
            if(elfo.getStatus()!= Status.MORTO){
                switch(elfo.getClass().getName()){
                    case "ElfoVerde":
                    retorno.add(0,elfo);
                    break;
                    case "ElfoNoturno":
                    retorno.add(elfo);
                    break;
                }
            }
        }
        return retorno;
    }

    private ArrayList<Elfo> collection(ArrayList<Elfo> elfos){
        Collections.sort(elfos, new Comparator<Elfo>(){
                public int compare(Elfo elfoAtual,Elfo elfoProximo){
                    boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();

                    if(mesmoTipo){
                        return 0;
                    }
                    return elfoAtual instanceof ElfoVerde && elfoProximo instanceof ElfoNoturno?-1:1;
                }
            });
        return elfos;
    }

}
