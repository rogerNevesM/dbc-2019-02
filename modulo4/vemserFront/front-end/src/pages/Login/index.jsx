import React, { Component } from 'react'
import Axios from 'axios'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.trocaValorState = this.trocaValorState.bind(this)
    }
    trocaValorState(e) {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    logar(e) {
        e.preventDefault()
        const { username, password } = this.state
        if (username && password) {
            Axios({
                method: 'post',
                url: 'http://localhost:8080/login',
                data: {
                        username,
                        password
                }
              }).then(resp => {                
                localStorage.setItem('Authorization', resp.headers.authorization)
                this.props.history.push('/')
            }
            ).catch(err=>{
                
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <section className='section-login'>
                    <div className='div-login'>
                        <h2>Logar</h2>
                        <input type="text" name="username" id="username" placeholder="username" onChange={this.trocaValorState} />
                        <input type="password" name="password" id="password" placeholder="Digite a password" onChange={this.trocaValorState} />
                        <button onClick={this.logar.bind(this)}>Logar</button>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}