import React, { Component, Fragment } from 'react'
import Axios from 'axios';
import ListaPersonagens from '../../components/ListaPersonagens'
import './css/home.css'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            elfos: [],
            dwarfs: [],
            personagens: []
        }
    }
    componentDidMount() {
        Axios.get('http://localhost:8080/api/elfo/', {
            headers:
            {
                Authorization: localStorage.getItem('Authorization')
            }
        }).then(resp => {
            const elfos = resp.data;
            this.setState({
                elfos
            })
            this.ordernar()
        })
        Axios.get('http://localhost:8080/api/dwarf/', {
            headers:
            {
                Authorization: localStorage.getItem('Authorization')
            }
        }).then(resp => {
            const dwarfs = resp.data;
            this.setState({
                dwarfs
            })
            this.ordernar()
        })
    }

    ordernar(){
        const {elfos,dwarfs} = this.state
        let {personagens} = this.state;
        personagens = elfos.concat(dwarfs);
        personagens = personagens.sort((a,b)=>{
            if(a.id<b.id)
                return -1
            else if(a.id>b.id)
                return +1
            return 0
        }
        )
        this.setState({personagens})
    }

    render() {
        const {personagens} = this.state
        return (
            <Fragment>
                <header><h1>Personagens</h1></header>
                <section>
                    <ListaPersonagens personagens={personagens}/>
                </section>
            </Fragment>
        )
    }
}