import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import Login from './pages/Login'
import Home from './pages/Home'

import PrivateRoute from './components/PrivateRoute'


function App() {
  return (
    <Router>
      <Route path='/login' component={Login} />
      <PrivateRoute path='/' exact  component={Home}/>
    </Router>
  );
}

export default App;
