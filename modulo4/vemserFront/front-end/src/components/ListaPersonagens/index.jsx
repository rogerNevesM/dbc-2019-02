import React, { Component, Fragment } from 'react'
import PersonagemCard from './../PersonagemCard'
import './css/formulario.css'

export default class ListaPersonagem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            formulario : false,
            novoNome:''
        }
    }

    abrirFecharFormulario(){
        const formulario = !this.state.formulario
        this.setState({
            formulario
        })
    }
    
    render() {
        const {personagens} = this.props
        const {formulario,novoNome} = this.state
        return (
            <Fragment>
                {
                    personagens.map((personagem,index)=>{
                        return(
                            <PersonagemCard personagem={personagem} key={index}/>
                        )
                    })
                }
                <button className="card" onClick={this.abrirFecharFormulario.bind(this)}>+</button>
                {formulario && (
                <Fragment>
                <div className="div-fechar" onClick={this.abrirFecharFormulario.bind(this)}></div>
                <div className="div-form">
                    <input type="text" value={novoNome}/>
                    <select name="" id="">
                        <option value="Elfo">elfo</option>
                        <option value="Elfo">dwarf</option>
                    </select>
                    <button>adicionar</button>
                </div>
                </Fragment>
                )}
            </Fragment>
        )
    }
}