import React, { Component, Fragment } from 'react'
import './css/card.css'
import ElfoImg from './img/Elfo.png'
import DwarfImg from './img/Dwarf.jpg'

export default class PersonagemCard extends Component{
    render(){
        const {personagem} = this.props
        console.log(personagem)
        return(
            <div className="card">
                <p>{`${personagem.id}: ${personagem.nome}`}</p>
                <img src={personagem.tipoPersonagem=='ELFO'?ElfoImg:DwarfImg}/>
                <p>Vida: {personagem.vida}</p>
                <p>Experiencia: {personagem.experiencia}</p>
            </div>
        )
    }
}