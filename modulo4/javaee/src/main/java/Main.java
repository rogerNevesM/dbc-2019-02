import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args){
        Connection conn = Connector.connect();
        try {
            //Paises
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                        +"ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                        +"NOME VARCHAR(100) NOT NULL\n"
                        +")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into paises(id_pais,nome) "
                    +"values(paises_seq.nextval,?)"
            );
            pst.setString(1,"Brasil");
            pst.executeUpdate();
            rs = conn.prepareStatement("select * from paises").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do Pais: %s", rs.getString("nome")));
            }
            //estados
            rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE ESTADOS(\n"
                        +" ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n"
                        +" NOME VARCHAR(100) NOT NULL,\n"
                        +" FK_ID_PAIS INTEGER NOT NULL,\n"
                        +"  FOREIGN KEY (FK_ID_PAIS)\n"
                        +"    REFERENCES PAISES(ID_PAIS)"
                        +")").execute();
            }
            pst = conn.prepareStatement("insert into estados(id_estado,nome,fk_id_pais) "
                    +"values(estados_seq.nextval,?,?)"
            );
            pst.setString(1,"Rio Grande do sul");
            pst.setInt(2,21);

            pst.executeUpdate();
            rs = conn.prepareStatement("select * from estados").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do estado: %s", rs.getString("nome")));
            }
            //Cidades

            rs = conn.prepareStatement("select tname from tab where tname = 'CIDADES'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE CIDADES(\n" +
                        "  ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n" +
                        "  NOME VARCHAR2(100) NOT NULL,\n" +
                        "  FK_ID_ESTADO INTEGER NOT NULL,\n" +
                        "  FOREIGN KEY (FK_ID_ESTADO)\n" +
                        "    REFERENCES ESTADOS(ID_ESTADO)\n" +
                        ")").execute();
            }
            pst = conn.prepareStatement("insert into cidades(id_cidade,nome,fk_id_estado) "
                    +"values(cidades_seq.nextval,?,?)"
            );
            pst.setString(1,"Charqueadas");
            pst.setInt(2,22);
            pst.executeUpdate();
            rs = conn.prepareStatement("select * from cidades").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome da cidade: %s", rs.getString("nome")));
            }
            //bairro
            rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'")
                    .executeQuery();
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE BAIRROS(\n" +
                        "  ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "  NOME VARCHAR2(100) NOT NULL,\n" +
                        "  FK_ID_CIDADE INTEGER NOT NULL,\n" +
                        "  FOREIGN KEY (FK_ID_CIDADE)\n" +
                        "    REFERENCES CIDADES(ID_CIDADE)\n" +
                        ")").execute();
            }
            pst = conn.prepareStatement("insert into bairros(id_bairro,nome,fk_id_cidade) "
                    +"values(bairros_seq.nextval,?,?)"
            );
            pst.setString(1,"vila otilia");
            pst.setInt(2,21);
            pst.executeUpdate();
            rs = conn.prepareStatement("select * from bairros").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do bairro: %s", rs.getString("nome")));
            }
        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO na consulta do main", ex);
        }
    }
}
