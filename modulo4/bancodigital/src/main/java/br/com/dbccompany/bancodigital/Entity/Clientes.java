package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
public class Clientes extends AbstractEntity {
	

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_CLIENTE")
	@GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String CPF;
	private String nome;
	private String RG;
	private String conjuge;
	private String dataNascimento;
	
	@Enumerated(EnumType.STRING)
	private EstadoCivil estadoCivil;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_endereco" )
	private Enderecos endereco;
	
	@OneToMany( mappedBy = "cliente", cascade = CascadeType.ALL )
	private List<Emails> emails = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Telefones> telefones = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes" )
	private List<Correntistas> correntistas = new ArrayList<>();

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String cPF) {
		CPF = cPF;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String rG) {
		RG = rG;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Enderecos getEndereco() {
		return endereco;
	}

	public void setEndereco(Enderecos endereco) {
		this.endereco = endereco;
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	
	
}
