package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {
	private Integer id;
	private String nome;
	private String cpf;
	private String rg;
	private String conjuge;
	private String dataNascimento;
	private EstadoCivil estadoCivil;
	private EnderecosDTO endereco;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getConjuge() {
		return conjuge;
	}
	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public EnderecosDTO getEndereco() {
		return endereco;
	}
	public void setEndereco(EnderecosDTO endereco) {
		this.endereco = endereco;
	}
	
}
