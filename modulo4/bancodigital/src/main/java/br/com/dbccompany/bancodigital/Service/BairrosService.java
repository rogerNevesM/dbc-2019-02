package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class BairrosService {
	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	private static final Logger LOG = Logger.getLogger( BairrosService.class.getName() );
	
	public void salvarBairro( Bairros bairros  ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(1);
			if( bairrosRes == null ) {
				BAIRROS_DAO.criar( bairros );
			}else {
				bairros.setId( bairrosRes.getId() );
				BAIRROS_DAO.atualizar( bairros );
			}
			
			if( started ) {
				transaction.commit();
			}
		}catch( Exception e ) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e );
		}
		
	}
	public void salvarBairro(BairrosDTO bairroDto) {
		Bairros bairro = BAIRROS_DAO.parseFrom(bairroDto);
		salvarBairro(bairro);
		bairroDto.setId(bairro.getId());
	}
}
