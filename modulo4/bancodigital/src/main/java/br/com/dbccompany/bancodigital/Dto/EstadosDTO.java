package br.com.dbccompany.bancodigital.Dto;

public class EstadosDTO {
	private Integer id;
	private String nome;
	
	private PaisesDTO pais;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PaisesDTO getPais() {
		return pais;
	}

	public void setPais(PaisesDTO pais) {
		this.pais = pais;
	}
	
	
}
