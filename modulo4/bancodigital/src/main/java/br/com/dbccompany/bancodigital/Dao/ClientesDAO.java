package br.com.dbccompany.bancodigital.Dao;


import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Enderecos;


public class ClientesDAO extends AbstractDAO<Clientes> {
	
private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	
	public Clientes parseFrom( ClientesDTO dto) {
		Clientes cliente = null;
		if( dto.getId() != null ) {
			cliente = buscar( dto.getId() );
		}else {
			cliente = new Clientes();
		}
		cliente.setNome(dto.getNome());
		cliente.setCPF(dto.getCpf());
		cliente.setRG(dto.getRg());
		cliente.setConjuge(dto.getConjuge());
		cliente.setDataNascimento(dto.getDataNascimento());
		cliente.setEstadoCivil(dto.getEstadoCivil());
		Enderecos endereco = ENDERECOS_DAO.parseFrom(dto.getEndereco());
		cliente.setEndereco(endereco);
		return cliente;
	}
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}
}
