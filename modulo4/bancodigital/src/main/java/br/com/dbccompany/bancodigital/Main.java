package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import br.com.dbccompany.bancodigital.Dao.PaisesDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		PaisesService service = new PaisesService();
		PaisesDTO pais = new PaisesDTO();
		pais.setNome("Brasil");
		service.salvarPaises(pais);
		
		
		EstadosService estadosService = new EstadosService();
		EstadosDTO estado = new EstadosDTO();
		estado.setNome("Rio grande do sul");
		estado.setPais(pais);
		estadosService.salvarEstados(estado);
		
		CidadesService cidadeService = new CidadesService();
		CidadesDTO cidade = new CidadesDTO();
		cidade.setEstado(estado);
		cidadeService.salvarCidades(cidade);
		
		BancosService bancosService = new BancosService();
		BancosDTO banco1 = new BancosDTO();
		banco1.setNome("banco");
		banco1.setCodigo(1);
		bancosService.salvarBancos(banco1);
		
		AgenciasService agenciasService = new AgenciasService();
		AgenciasDTO agencia1 = new AgenciasDTO();
		agencia1.setNome("ola");
		agencia1.setCodigo(1);
		agencia1.setBanco(banco1);
		agenciasService.salvarAgencias(agencia1);
		 
		List<AgenciasDTO> agencias = new ArrayList<>();
		agencias.add(agencia1);
		
		
		CorrentistasService correntistasService = new CorrentistasService();
		CorrentistasDTO correntista = new CorrentistasDTO();
		correntista.depositar(100.00);
		correntista.setAgencias(agencias);
		System.out.println(correntista.getSaldo());
		
		
		CorrentistasDTO correntista2 = new CorrentistasDTO();
		correntista2.depositar(100.00);
		correntista2.setAgencias(agencias);
		System.out.println(correntista2.getSaldo());
		correntista.tranferir(100.0, correntista2);
		System.out.println(correntista2.getSaldo());
		System.out.println(correntista.getSaldo());
		
		correntistasService.salvar(correntista);
		correntistasService.salvar(correntista2);
	
		
		System.exit(0);
	}
	
//	public static void main(String[] args) {
//		Session session = null;
//		Transaction transaction = null;
//		try {
//			session = HibernateUtil.getSession();
//			transaction = session.beginTransaction();
//			
//			Paises pais = new Paises();
//			pais.setNome("Brasil");
//			
//			session.save(pais);
//			//session.createQuery("select * from paises").executeUpdate();
//			Criteria criteria = session.createCriteria(Paises.class);
//			criteria.createAlias("nome", "nome_paises");
//			criteria.add(
//					Restrictions.isNotNull("nome")					
//			);
//			
//			List<Paises> lstPaises = criteria.list();
//			
//			transaction.commit();
//			
//		}catch (Exception e) {
//			if( transaction != null ) {
//				transaction.rollback();
//			}
//			LOG.log(Level.SEVERE, e.getMessage(),e);
//			System.exit(1);
//		}finally {
//			if(session != null ) {
//				session.close();
//			}
//		}
//		System.exit(0);
//	}
}
