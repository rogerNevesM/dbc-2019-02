package br.com.dbccompany.bancodigital.Entity;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.*;


@Entity
@SequenceGenerator( allocationSize = 1, name = "PAISES_SEQ", sequenceName = "PAISES_SEQ" )
public class Paises extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name = "ID_PAIS")
	@GeneratedValue( generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	private String nome;
	
	@OneToMany( mappedBy = "pais", cascade = CascadeType.ALL )
	private List<Estados> estados = new ArrayList<>();
	
	public List<Estados> getEstados() {
		return estados;
	}
	public void setEstados(List<Estados> estados) {
		this.estados = estados;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	
}
