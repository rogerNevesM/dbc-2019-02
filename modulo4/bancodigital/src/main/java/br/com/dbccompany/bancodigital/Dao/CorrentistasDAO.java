package br.com.dbccompany.bancodigital.Dao;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas>{
	
	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();

	
	public Correntistas parseFrom( CorrentistasDTO dto) {
		Correntistas correntista = null;
		if( dto.getId() != null ) {
			correntista = buscar( dto.getId() );
		}else {
			correntista = new Correntistas();
		}
		correntista.setCNPJ(dto.getCNPJ());
		correntista.setRazaoSocial(dto.getRazaoSocial());
		correntista.setTipo(dto.getTipo());
		correntista.setSaldo(dto.getSaldo());
		List<Agencias> agencias = new ArrayList<>();
		for (AgenciasDTO agencia : dto.getAgencias()) {
			agencias.add(AGENCIAS_DAO.parseFrom(agencia));
		}
		correntista.setAgencias(agencias);
//		List<Clientes> clientes = new ArrayList<>();
//		for (ClientesDTO cliente : dto.getClientes()) {
//			clientes.add(CLIENTES_DAO.parseFrom(cliente));
//		}
//		correntista.setClientes(clientes);
		return correntista;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
}
