package br.com.dbccompany.bancodigital.Dto;

import java.util.List;

import br.com.dbccompany.bancodigital.Entity.CorrentistaType;

public class CorrentistasDTO {

	private Integer id;
	private String razaoSocial;
	private String CNPJ;
	private Double saldo = 0.0;
	private CorrentistaType tipo;
	private List<ClientesDTO> clientes;
	private List<AgenciasDTO> agencias;

	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	public String getCNPJ() {
		return CNPJ;
	}
	
	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}
	
	public CorrentistaType getTipo() {
		return tipo;
	}
	
	public void setTipo(CorrentistaType tipo) {
		this.tipo = tipo;
	}
	
	public Double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public List<ClientesDTO> getClientes() {
		return clientes;
	}
	
	public void setClientes(List<ClientesDTO> clientes) {
		this.clientes = clientes;
	}
	
	public List<AgenciasDTO> getAgencias() {
		return agencias;
	}
	
	public void setAgencias(List<AgenciasDTO> agencias) {
		this.agencias = agencias;
	}
	
	public void depositar(Double valor) {
		saldo +=valor;
	}
	
	public boolean sacar(Double valor) {
		if(saldo>=valor) {
			saldo -=valor;
			return true;
		}
		return false;
	}
	
	public boolean tranferir(Double valor, CorrentistasDTO destinatario){
		if(this.sacar(valor)) {
			destinatario.depositar(valor);
			return true;
		}
		return false;
	}
}
