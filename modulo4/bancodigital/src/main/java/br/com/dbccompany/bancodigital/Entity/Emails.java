package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "EMAILS_SEQ", sequenceName = "EMAILS_SEQ" )
public class Emails extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_EMAIL")
	@GeneratedValue( generator = "EMAILS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String Email;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_cliente" )
	private Clientes cliente;

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	@Override
	public Integer getId() {
		
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	
}
