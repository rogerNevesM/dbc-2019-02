package br.com.dbccompany.bancodigital.Dto;

public class CidadesDTO {
	
	private Integer id;
	private String nome;
	private EstadosDTO estado;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public EstadosDTO getEstado() {
		return estado;
	}
	public void setEstado(EstadosDTO estado) {
		this.estado = estado;
	}
	
}
