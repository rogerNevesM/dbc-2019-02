package br.com.dbccompany.bancodigital.Dto;

public class BairrosDTO {
	
	private Integer id;
	private String nome;
	private CidadesDTO cidade;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public CidadesDTO getCidade() {
		return cidade;
	}
	public void setCidade(CidadesDTO cidade) {
		this.cidade = cidade;
	}
	
	
}
