package br.com.dbccompany.bancodigital.Dto;

public class AgenciasDTO {
	private Integer id;
	private Integer codigo;
	private String nome;
	private BancosDTO banco;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BancosDTO getBanco() {
		return banco;
	}
	public void setBanco(BancosDTO banco) {
		this.banco = banco;
	}
	
}
