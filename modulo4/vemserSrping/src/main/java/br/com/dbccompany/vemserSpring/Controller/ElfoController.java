package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Elfo;
import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.ElfoService;
import br.com.dbccompany.vemserSpring.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/elfo")
public class ElfoController {

    @Autowired
    ElfoService elfoService;
    @Autowired
    InventarioService inventarioService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Elfo> todosElfos(){
        List<Elfo> elfos = elfoService.todosElfos();
        for (Elfo elfo: elfos) {
            if(elfo.getInventario()!=null) {
                elfo.getInventario().setPersonagem(null);
                if (elfo.getInventario().getInventarioXItems()!=null){
                    for (InventarioXItem inventarioXItem: elfo.getInventario().getInventarioXItems()) {
                        inventarioXItem.setInventario(null);
                        if (inventarioXItem.getItem() != null)
                            inventarioXItem.getItem().setInventarioXItems(null);
                    }
                }
            }
        }
        return elfos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Elfo novoElfo(@RequestBody Elfo elfo){
        elfo = elfoService.salvar(elfo);
        if(elfo.getInventario()!=null) {
            elfo.getInventario().setPersonagem(null);
            if (elfo.getInventario().getInventarioXItems()!=null){
                for (InventarioXItem inventarioXItem: elfo.getInventario().getInventarioXItems()) {
                    inventarioXItem.setInventario(null);
                    if (inventarioXItem.getItem() != null)
                        inventarioXItem.getItem().setInventarioXItems(null);
                }
            }
        }
        return elfo;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Elfo editarElfo(@PathVariable Integer id,@RequestBody Elfo elfo) {
        elfo = elfoService.editar(id, elfo);
        if(elfo.getInventario()!=null) {
            elfo.getInventario().setPersonagem(null);
            if (elfo.getInventario().getInventarioXItems()!=null){
                for (InventarioXItem inventarioXItem: elfo.getInventario().getInventarioXItems()) {
                    inventarioXItem.setInventario(null);
                    if (inventarioXItem.getItem() != null)
                        inventarioXItem.getItem().setInventarioXItems(null);
                }
            }
        }
        return elfo;
    }
}
