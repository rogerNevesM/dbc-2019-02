package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Dwarf;
import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Repository.DwarfRepository;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DwarfService {

    @Autowired
    private DwarfRepository dwarfRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    @Transactional(rollbackFor = Exception.class)
    public Dwarf salvar(Dwarf dwarf){
        Inventario inventarioObject = dwarf.getInventario();
        dwarf = dwarfRepository.save(dwarf);
        if(inventarioObject.getId() == null){
            inventarioObject.setPersonagem(dwarf);
            inventarioObject = inventarioRepository.save(inventarioObject);
        }
        dwarf.setInventario(inventarioObject);
        return dwarf;
    }

    @Transactional(rollbackFor = Exception.class)
    public Dwarf editar(Integer id, Dwarf dwarf){
        dwarf.setId(id);
        return this.salvar(dwarf);
    }

    public List<Dwarf> todosDwarfs(){
        return (List<Dwarf>) dwarfRepository.findAll();

    }

}
