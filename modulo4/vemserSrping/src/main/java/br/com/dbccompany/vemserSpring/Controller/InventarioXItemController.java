package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.InventarioService;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import br.com.dbccompany.vemserSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/api/inventarioXitem")
public class InventarioXItemController {

    @Autowired
    InventarioXItemService inventarioXItemService;
    @Autowired
    InventarioService inventarioService;
    @Autowired
    ItemService itemService;

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInventarioXItem(@RequestBody InventarioXItem inventarioXItem){
        inventarioXItem = inventarioXItemService.salvar(inventarioXItem);
        if (inventarioXItem.getItem() != null)
            inventarioXItem.getItem().setInventarioXItems(null);
        if (inventarioXItem.getInventario() != null)
            inventarioXItem.getInventario().setInventarioXItems(null);
        if (inventarioXItem.getInventario().getPersonagem() != null)
            inventarioXItem.getInventario().getPersonagem().setInventario(null);
        return inventarioXItem;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarInventarioXItem(@PathVariable Integer id, @RequestBody InventarioXItem inventarioXItem){
        inventarioXItem = inventarioXItemService.editar(id, inventarioXItem);
        inventarioXItem.getItem().setInventarioXItems(null);
        inventarioXItem.getInventario().setInventarioXItems(null);
        if (inventarioXItem.getInventario().getPersonagem() != null)
            inventarioXItem.getInventario().getPersonagem().setInventario(null);
        return inventarioXItem;
    }
}
