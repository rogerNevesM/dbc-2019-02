package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Elfo;
import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Repository.ElfoRepository;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ElfoService {

    @Autowired
    private ElfoRepository elfoRepository;

    @Autowired
    private InventarioRepository inventarioRepository;

    @Transactional(rollbackFor = Exception.class)
    public Elfo salvar(Elfo elfo){
        Inventario inventarioObject = elfo.getInventario();
        elfo = elfoRepository.save(elfo);
        if(inventarioObject.getId() == null){
            inventarioObject.setPersonagem(elfo);
            inventarioObject = inventarioRepository.save(inventarioObject);
        }
        elfo.setInventario(inventarioObject);

        return elfo;
    }

    @Transactional(rollbackFor = Exception.class)
    public Elfo editar(Integer id, Elfo elfo){
        elfo.setId(id);
        return this.salvar(elfo);
    }

    public List<Elfo> todosElfos(){
        return (List<Elfo>) elfoRepository.findAll();
    }
}
