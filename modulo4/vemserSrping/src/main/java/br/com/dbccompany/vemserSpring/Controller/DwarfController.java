package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Dwarf;
import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.DwarfService;
import br.com.dbccompany.vemserSpring.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/dwarf")
public class DwarfController {

    @Autowired
    DwarfService dwarfService;
    @Autowired
    InventarioService inventarioService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Dwarf> todosDwarfs(){
        List<Dwarf> dwarfs = dwarfService.todosDwarfs();
        for (Dwarf dwarf: dwarfs) {
            if(dwarf.getInventario()!=null) {
                dwarf.getInventario().setPersonagem(null);
                if (dwarf.getInventario().getInventarioXItems()!=null){
                    for (InventarioXItem inventarioXItem: dwarf.getInventario().getInventarioXItems()) {
                        inventarioXItem.setInventario(null);
                        if (inventarioXItem.getItem() != null)
                            inventarioXItem.getItem().setInventarioXItems(null);
                    }
                }
            }
        }
        return dwarfs;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Dwarf novoDwarf(@RequestBody Dwarf dwarf){
        dwarf = dwarfService.salvar(dwarf);
        if(dwarf.getInventario()!=null) {
            dwarf.getInventario().setPersonagem(null);
            if (dwarf.getInventario().getInventarioXItems()!=null){
                for (InventarioXItem inventarioXItem: dwarf.getInventario().getInventarioXItems()) {
                    inventarioXItem.setInventario(null);
                    if (inventarioXItem.getItem() != null)
                        inventarioXItem.getItem().setInventarioXItems(null);
                }
            }
        }
        return dwarf;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Dwarf editarDwarf(@PathVariable Integer id,@RequestBody Dwarf dwarf) {
        dwarf = dwarfService.editar(id,dwarf);
        if(dwarf.getInventario()!=null) {
            dwarf.getInventario().setPersonagem(null);
            if (dwarf.getInventario().getInventarioXItems()!=null){
                for (InventarioXItem inventarioXItem: dwarf.getInventario().getInventarioXItems()) {
                    inventarioXItem.setInventario(null);
                    if (inventarioXItem.getItem() != null)
                        inventarioXItem.getItem().setInventarioXItems(null);
                }
            }
        }
        return dwarf;
    }
}
