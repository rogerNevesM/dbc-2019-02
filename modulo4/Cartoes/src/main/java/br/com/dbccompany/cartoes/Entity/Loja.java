package br.com.dbccompany.cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ" )
public class Loja {
	
	@Id
	@Column(name = "ID_LOJA")
	@GeneratedValue( generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String nome;
}
