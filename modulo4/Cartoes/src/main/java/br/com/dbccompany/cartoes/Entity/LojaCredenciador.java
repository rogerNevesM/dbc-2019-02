package br.com.dbccompany.cartoes.Entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class LojaCredenciador {
	//id composto
	@EmbeddedId
	private LojaCredenciadorId id;
	
	private Double taxa;

	public LojaCredenciadorId getId() {
		return id;
	}

	public void setId(LojaCredenciadorId id) {
		this.id = id;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}

	
	
	
}
