package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Controller.UsuariosController;
import br.com.dbccompany.TabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.TabalhoFinal.TrabalhoFinalApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class TipoContatoIntegrationTest extends TrabalhoFinalApplicationTests {
    private MockMvc mock;

    @Autowired
    private UsuariosController controller;

    @Before
    public void setUp(){
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetApiStatusOk() throws Exception{
        this.mock.perform(MockMvcRequestBuilders.get("/api/usuario/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
