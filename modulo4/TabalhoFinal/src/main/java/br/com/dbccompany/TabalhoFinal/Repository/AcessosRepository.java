package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
    public List<Acessos> findAllBySaldoCliente_idClienteAndSaldoCliente_idEspaco(SaldoCliente saldoCliente);
}
