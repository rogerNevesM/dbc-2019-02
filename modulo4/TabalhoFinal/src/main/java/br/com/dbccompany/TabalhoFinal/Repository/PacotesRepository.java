package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {
}
