package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Espacos;
import br.com.dbccompany.TabalhoFinal.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espacos salvar(Espacos espaco) {
       return repository.save(espaco);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espacos editar(Espacos espaco, Integer id) {
        espaco.setId(id);
        return this.salvar(espaco);
    }

    public List<Espacos> listarTodos(){
        return (List<Espacos>) repository.findAll();
    }
}
