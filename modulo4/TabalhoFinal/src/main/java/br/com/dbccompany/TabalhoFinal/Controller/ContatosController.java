package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Contatos;
import br.com.dbccompany.TabalhoFinal.Service.ContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatosController {

    @Autowired
    ContatosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contatos> listarTodos() {
        List<Contatos> contatos = service.listarTodos();
        return contatos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contatos novo(@RequestBody Contatos contato) {
        contato = service.salvar(contato);
        return contato;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contatos editar(@PathVariable Integer id, @RequestBody Contatos contato) {
        contato = service.editar(contato,id);
        return contato;
    }

}
