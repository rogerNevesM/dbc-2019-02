package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Clientes;
import br.com.dbccompany.TabalhoFinal.Entity.Contatos;
import br.com.dbccompany.TabalhoFinal.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Autowired
    private ContatosService contatosService;

    @Transactional(rollbackFor = Exception.class)
    public Clientes salvar(Clientes cliente) {
        boolean testeTiposContatos = false;
        List<Contatos> contatos = cliente.getContatos();
        for (Contatos contato: contatos) {
            if(contato.getTipoContato().getNome().equalsIgnoreCase("Telefone")){
                for (Contatos contato2: contatos) {
                    if(contato2.getTipoContato().getNome().equalsIgnoreCase("email"))
                        testeTiposContatos = true;
                }
            }
        }
        if(testeTiposContatos){
            cliente =repository.save(cliente);
            for (Contatos contatoSalvar: contatos) {
                if(contatoSalvar.getId()==null){
                    contatoSalvar.setCliente(cliente);
                    contatoSalvar = contatosService.salvar(contatoSalvar);
                }
            }
            cliente.setContatos(contatos);
        }
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public Clientes editar(Clientes cliente, Integer id) {
        cliente.setId(id);
        return this.salvar(cliente);
    }

    public List<Clientes> listarTodos(){
        return (List<Clientes>) repository.findAll();
    }
}
