package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.TabalhoFinal.Entity.SaldoClienteId;
import br.com.dbccompany.TabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TabalhoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClientesService {
    @Autowired
    private SaldoClienteRepository repository;

    public void salvar(Integer idCliente, Integer idEspaco, TipoContratacao tipoContratacao, Integer quantidade,Integer prazo){
        SaldoClienteId id = new SaldoClienteId(idCliente,idEspaco);
        SaldoCliente saldoCliente = this.buscar(id);
        if(saldoCliente == null){
            saldoCliente = new SaldoCliente();
            SaldoClienteId saldoClienteId = new SaldoClienteId(idCliente,idEspaco);
            saldoCliente.setId(saldoClienteId);
        }
        Calendar calendar = Calendar.getInstance();
        if(saldoCliente.getVencimento()==null || calendar.after(saldoCliente.getVencimento())){
            saldoCliente.setVencimento(calendar.getTime());
        }
        calendar.setTime(saldoCliente.getVencimento());
        switch (tipoContratacao){
            case HORAS:
                calendar.add(Calendar.HOUR,quantidade+prazo);
                break;
            case DIARIAS:
                calendar.add(Calendar.DAY_OF_YEAR,quantidade+prazo);
                break;
            case MESES:
                calendar.add(Calendar.MONTH,quantidade+prazo);
                break;
            case MINUTOS:
                calendar.add(Calendar.MINUTE,quantidade+prazo);
                break;
            case SEMANAS:
                calendar.add(Calendar.WEEK_OF_YEAR,quantidade+prazo);
                break;
            case TURNOS:
                calendar.add(Calendar.HOUR,(quantidade*5)+prazo);
                break;
        }
        saldoCliente.setVencimento(calendar.getTime());
        saldoCliente.setTipoContratacao(tipoContratacao);
        if (saldoCliente.getQuantidade() == null){
            saldoCliente.setQuantidade(0);
        }
        saldoCliente.setQuantidade(saldoCliente.getQuantidade()+quantidade);
        repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoCliente saldoCliente) {
        return this.salvar(saldoCliente);
    }

    public List<SaldoCliente> listarTodos(){
        return (List<SaldoCliente>) repository.findAll();
    }

    public SaldoCliente buscar(SaldoClienteId id){
        Optional<SaldoCliente> saldoCliente = repository.findById(id);
        if(saldoCliente == null)
            return null;
        return saldoCliente.get();
    }
}
