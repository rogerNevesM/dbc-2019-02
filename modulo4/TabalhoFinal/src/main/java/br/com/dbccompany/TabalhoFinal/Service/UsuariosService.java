package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.TabalhoFinal.Repository.UsuariosRepository;
import br.com.dbccompany.TabalhoFinal.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios usuario) throws NoSuchAlgorithmException {
        String senha = usuario.getSenha();
        if (senha.length() >= 6) {
            String novaSenha = Util.MD5(senha);
            usuario.setSenha(novaSenha);
            return usuariosRepository.save(usuario);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuarios editar(Usuarios usuarioNovo, String senhaAntiga) throws NoSuchAlgorithmException {
        if (senhaAntiga.length() >= 6) {
            Optional<Usuarios> busca = usuariosRepository.findById(usuarioNovo.getId());
            if (busca != null) {
                Usuarios usuarioAntigo = busca.get();
                if (usuarioAntigo.getSenha() == Util.MD5(senhaAntiga)){
                    return this.salvar(usuarioNovo);
                }
            }
        }
        return null;
    }

    public List<Usuarios> listarTodos(){
        return (List<Usuarios>) usuariosRepository.findAll();
    }
}
