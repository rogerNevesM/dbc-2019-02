package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.TabalhoFinal.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> listarTodos() {
        List<Pacotes> pacotes = service.listarTodos();
        return pacotes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacotes novo(@RequestBody Pacotes pacote) {
        pacote = service.salvar(pacote);
        return pacote;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pacotes editar(@PathVariable Integer id, @RequestBody Pacotes pacote) {
        pacote = service.editar(pacote,id);
        return pacote;
    }
}
