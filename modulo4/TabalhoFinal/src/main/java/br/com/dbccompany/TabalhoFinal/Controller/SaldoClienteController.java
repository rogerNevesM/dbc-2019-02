package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.TabalhoFinal.Service.SaldoClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    SaldoClientesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> listarTodos() {
        List<SaldoCliente> saldoClientes = service.listarTodos();
        return saldoClientes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoCliente novo(@RequestBody SaldoCliente saldoCliente) {
        saldoCliente = service.salvar(saldoCliente);
        return saldoCliente;
    }

    @PutMapping(value = "/editar")
    @ResponseBody
    public SaldoCliente editar( @RequestBody SaldoCliente saldoCliente) {
        saldoCliente = service.editar(saldoCliente);
        return saldoCliente;
    }
}
