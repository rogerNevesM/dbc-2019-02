package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Clientes;
import br.com.dbccompany.TabalhoFinal.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> listarTodos() {
        List<Clientes> clientes = service.listarTodos();
        return clientes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes novo(@RequestBody Clientes cliente) {
        cliente = service.salvar(cliente);
        return cliente;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Clientes editar(@PathVariable Integer id, @RequestBody Clientes cliente) {
        cliente = service.editar(cliente,id);
        return cliente;
    }
}
