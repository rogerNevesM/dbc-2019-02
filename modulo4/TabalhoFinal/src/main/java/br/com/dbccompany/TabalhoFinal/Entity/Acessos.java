package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acessos.class)
public class Acessos {
    @Id
    @Column(name = "ACESSOS_ID")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private boolean isEntrada;
    private Date data;
    private boolean isExcecao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns(value ={
            @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE", nullable = false),
            @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE", nullable = false) })
    private SaldoCliente saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
