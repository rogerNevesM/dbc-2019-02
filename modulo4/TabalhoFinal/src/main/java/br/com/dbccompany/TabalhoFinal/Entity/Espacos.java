package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Espacos.class)
public class Espacos {
    @Id
    @Column(name = "ESPACO_ID")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(unique=true,  nullable = false)
    private String nome;
    @Column( nullable = false)
    private Integer qtdPessoas;
    @Column( nullable = false)
    private Double valor;


    @OneToMany(mappedBy = "espaco",cascade = CascadeType.ALL)
    @Column( nullable = false)
    private List<EspacosPacotes> EspacosPacotes = new ArrayList<>();


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return this.qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return this.EspacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> EspacosPacotes) {
        this.EspacosPacotes = EspacosPacotes;
    }

}
