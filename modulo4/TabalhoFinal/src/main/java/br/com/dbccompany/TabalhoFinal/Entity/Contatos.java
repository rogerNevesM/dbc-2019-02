package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CONTATOS_SEQ", sequenceName = "CONTATOS_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Contatos.class)
public class Contatos {

    @Id
    @Column(name = "CONTATOS_ID")
    @GeneratedValue(generator = "CONTATOS_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;

    @ManyToOne
    @JoinColumn(name = "id_tipo_contato")
    private TipoContato tipoContato;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return this.tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }
}
