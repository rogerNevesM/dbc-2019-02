package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacosPacotes.class)
public class EspacosPacotes{
    @Id
    @Column(name = "ESPACOS_PACOTES_ID")
    @GeneratedValue(generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private Integer quantidade;
    private Integer prazo;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;


    @ManyToOne
    @JoinColumn(name = "id_pacote")
    private Pacotes pacote;


    @ManyToOne
    @JoinColumn(name = "id_espaco")
    private Espacos espaco;
    

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return this.prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacao getTipoContratacao() {
        return this.tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Pacotes getPacote() {
        return this.pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Espacos getEspaco() {
        return this.espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }
    
    
}