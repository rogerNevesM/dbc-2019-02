package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.TabalhoFinal.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {
    @Autowired
    private ContratacaoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Contratacao salvar(Contratacao contratacao) {
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editar(Contratacao contratacao, Integer id) {
        contratacao.setId(id);
        return this.salvar(contratacao);
    }

    public List<Contratacao> listarTodos(){
        return (List<Contratacao>) repository.findAll();
    }
}
