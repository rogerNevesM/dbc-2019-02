package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.TabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.TabalhoFinal.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacotesService {
    @Autowired
    private PacotesRepository repository;

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    @Transactional(rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacote) {
        Double valor = 0.0;
        pacote = repository.save(pacote);
        List<EspacosPacotes> espacosPacotes = pacote.getEspacosPacotes();
        for (EspacosPacotes espacosPacote: espacosPacotes) {
            espacosPacote.setPacote(pacote);
            espacosPacote = espacosPacotesService.salvar(espacosPacote);
            valor += espacosPacote.getQuantidade() * espacosPacote.getEspaco().getValor();
        }
        pacote.setValor(valor);
        return repository.save(pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacotes editar(Pacotes pacote, Integer id) {
        pacote.setId(id);
        return this.salvar(pacote);
    }

    public List<Pacotes> listarTodos(){
        return (List<Pacotes>) repository.findAll();
    }
}
