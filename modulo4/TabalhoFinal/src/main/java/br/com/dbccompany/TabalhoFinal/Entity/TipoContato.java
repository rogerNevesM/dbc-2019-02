package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoContato.class)
public class TipoContato {

    @Id
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ",strategy = GenerationType.SEQUENCE)
    @Column(name = "TIPO_CONTATO_ID")
    private Integer id;
    private String nome;

    @OneToMany(mappedBy = "tipoContato", cascade = CascadeType.ALL)
    private List<Contatos> contatos = new ArrayList<>();


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contatos> getContatos() {
        return this.contatos;
    }

    public void setContatos(List<Contatos> contatos) {
        this.contatos = contatos;
    }
    

}
