package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Espacos;
import br.com.dbccompany.TabalhoFinal.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> listarTodos() {
        List<Espacos> espacos = service.listarTodos();
        return espacos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novo(@RequestBody Espacos espaco) {
        espaco = service.salvar(espaco);
        return espaco;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espacos editar(@PathVariable Integer id, @RequestBody Espacos espaco) {
        espaco = service.editar(espaco,id);
        return espaco;
    }
}
