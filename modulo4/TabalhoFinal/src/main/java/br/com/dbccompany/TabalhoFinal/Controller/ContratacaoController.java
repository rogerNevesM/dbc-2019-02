package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.TabalhoFinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> listarTodos() {
        List<Contratacao> Contratacoes = service.listarTodos();
        return Contratacoes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contratacao novo(@RequestBody Contratacao contratacao) {
        contratacao = service.salvar(contratacao);
        return contratacao;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editar(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
        contratacao = service.editar(contratacao,id);
        return contratacao;
    }
}
