package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.TabalhoFinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> listarTodos() {
        List<TipoContato> tiposContatos = service.listarTodos();
        return tiposContatos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContato novo(@RequestBody TipoContato tipoContato) {
        tipoContato = service.salvar(tipoContato);
        return tipoContato;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContato editar(@PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        tipoContato = service.editar(tipoContato,id);
        return tipoContato;
    }
}
