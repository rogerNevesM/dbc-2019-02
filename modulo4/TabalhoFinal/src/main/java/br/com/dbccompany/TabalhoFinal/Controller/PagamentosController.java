package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Pagamentos;
import br.com.dbccompany.TabalhoFinal.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> listarTodos() {
        List<Pagamentos> pagamentos = service.listarTodos();
        return pagamentos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamentos novo(@RequestBody Pagamentos pacote) {
        pacote = service.salvar(pacote);
        return pacote;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pagamentos editar(@PathVariable Integer id, @RequestBody Pagamentos pacote) {
        pacote = service.editar(pacote,id);
        return pacote;
    }
}
