package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.TabalhoFinal.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesPacotesService {
    @Autowired
    private ClientesPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientePacote) {
        if((clientePacote.getCliente() == null || clientePacote.getPacote()==null) || clientePacote.getQuantidade()==null)
            return null;
        return repository.save(clientePacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes editar(ClientesPacotes clientePacote, Integer id) {
        clientePacote.setId(id);
        return this.salvar(clientePacote);
    }

    public List<ClientesPacotes> listarTodos(){
        return (List<ClientesPacotes>) repository.findAll();
    }
}
