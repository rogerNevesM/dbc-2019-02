package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Acessos;
import br.com.dbccompany.TabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.TabalhoFinal.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.time.Period; 
import java.time.temporal.ChronoUnit; 

@Service
public class AcessosService {
    @Autowired
    private AcessosRepository repository;

    @Autowired
    private SaldoClientesService saldoClientesService;

    @Transactional(rollbackFor = Exception.class)
    public Acessos salvar(Acessos acesso) {
        SaldoCliente saldoCliente =  saldoClientesService.buscar(acesso.getSaldoCliente().getId());
        if(acesso.getData()==null){
            acesso.setData(Calendar.getInstance().getTime());
        }
        if(acesso.isEntrada()){
            Calendar vencimento  = Calendar.getInstance();
            vencimento.setTime(saldoCliente.getVencimento());
            Calendar dataEntrada = Calendar.getInstance();
            dataEntrada.setTime(acesso.getData());
            if (!dataEntrada.before(vencimento) || saldoCliente.getQuantidade()<=0){
                return null;
            }
        }
        else{
            List<Acessos> acessos = repository.findAllBySaldoCliente_idClienteAndSaldoCliente_idEspaco(acesso.getSaldoCliente());
            Acessos ultimoAcesso = acessos.get(acessos.size()-1);
            LocalDate entrada = new LocalDate(ultimoAcesso.getData());
            LocalDate saida = new LocalDate(acesso.getData());
            Period intervalo = Period.between(entrada, saida);
            
            switch (saldoCliente.getTipoContratacao()){
                case HORAS:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-intervalo.get(ChronoUnit.HOURS));
                    break;
                case DIARIAS:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-intervalo.get(ChronoUnit.DAYS));
                    break;
                case MESES:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-intervalo.get(ChronoUnit.MONTHS));
                    break;
                case MINUTOS:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-intervalo.get(ChronoUnit.MINUTES));
                    break;
                case SEMANAS:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-(intervalo.get(ChronoUnit.DAYS)/7));
                    break;
                case TURNOS:
                    saldoCliente.setQuantidade(saldoCliente.getQuantidade()-intervalo.get(ChronoUnit.HOURS));
                    break;
            }
        }
        return repository.save(acesso);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Acessos acesso, Integer id) {
        acesso.setId(id);
        return this.salvar(acesso);
    }

    public List<Acessos> listarTodos(){
        return (List<Acessos>) repository.findAll();
    }
}
