package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.TabalhoFinal.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientesPacotes> listarTodos() {
        List<ClientesPacotes> clientesPacotes = service.listarTodos();
        return clientesPacotes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientesPacotes novo(@RequestBody ClientesPacotes clientePacote) {
        clientePacote = service.salvar(clientePacote);
        return clientePacote;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotes editar(@PathVariable Integer id, @RequestBody ClientesPacotes clientePacote) {

        clientePacote = service.editar(clientePacote,id);
        return clientePacote;
    }
}
