package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.TabalhoFinal.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {
}
