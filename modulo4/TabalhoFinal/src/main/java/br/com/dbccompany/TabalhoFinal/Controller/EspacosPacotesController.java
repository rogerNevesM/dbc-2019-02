package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.TabalhoFinal.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacosPacotes> listarTodos() {
        List<EspacosPacotes> espacosPacotes = service.listarTodos();
        return espacosPacotes;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacosPacotes novo(@RequestBody EspacosPacotes espacoPacote) {
        espacoPacote = service.salvar(espacoPacote);
        return espacoPacote;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacosPacotes editar(@PathVariable Integer id, @RequestBody EspacosPacotes espacoPacote) {
        espacoPacote = service.editar(espacoPacote,id);
        return espacoPacote;
    }
}
