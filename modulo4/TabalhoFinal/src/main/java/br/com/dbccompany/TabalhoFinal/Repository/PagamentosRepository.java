package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {
}
