package br.com.dbccompany.TabalhoFinal.Entity;

public enum TipoContratacao{
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}