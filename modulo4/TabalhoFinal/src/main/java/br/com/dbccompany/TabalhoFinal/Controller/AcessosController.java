package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Acessos;
import br.com.dbccompany.TabalhoFinal.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> listarTodos() {
        List<Acessos> acessos = service.listarTodos();
        return acessos;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Acessos novo(@RequestBody Acessos acesso) {
        acesso = service.salvar(acesso);
        return acesso;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Acessos editar(@PathVariable Integer id, @RequestBody Acessos acesso) {
        acesso = service.editar(acesso,id);
        return acesso;
    }

}
