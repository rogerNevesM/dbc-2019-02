package br.com.dbccompany.TabalhoFinal;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util {
    public static String MD5(String string) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(string.getBytes(),0,string.length());
        return new BigInteger(1,m.digest()).toString(16);
    }

}
