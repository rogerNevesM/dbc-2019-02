package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
}
