package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Espacos;
import br.com.dbccompany.TabalhoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.TabalhoFinal.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosPacotesService {
    @Autowired
    private EspacosPacotesRepository repository;
    @Autowired
    private EspacosService espacosService;

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacoPacote) {
        Espacos espaco = espacoPacote.getEspaco();
        if(espaco!=null && espaco.getId()==null){
            espaco = espacosService.salvar(espaco);
        }
        espacoPacote.setEspaco(espaco);
        return repository.save(espacoPacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes editar(EspacosPacotes espacoPacote, Integer id) {
        espacoPacote.setId(id);
        return this.salvar(espacoPacote);
    }

    public List<EspacosPacotes> listarTodos(){
        return (List<EspacosPacotes>) repository.findAll();
    }
}
