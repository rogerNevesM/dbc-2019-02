package br.com.dbccompany.TabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator( allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
public class Usuarios {

    @Id
    @Column(name = "ID_USUARIO")
    @GeneratedValue(generator = "USUARIOS_SEQ",strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String nome;

    @Column(unique=true,  nullable = false)
    private String email;

    @Column(unique=true ,nullable = false)
    private String login;

    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
