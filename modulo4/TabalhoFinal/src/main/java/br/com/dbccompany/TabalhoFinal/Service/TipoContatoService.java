package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.TabalhoFinal.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato tipoContato) {
        return repository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato editar(TipoContato tipoContato, Integer id) {
        tipoContato.setId(id);
        return this.salvar(tipoContato);
    }

    public List<TipoContato> listarTodos(){
        return (List<TipoContato>) repository.findAll();
    }
}
