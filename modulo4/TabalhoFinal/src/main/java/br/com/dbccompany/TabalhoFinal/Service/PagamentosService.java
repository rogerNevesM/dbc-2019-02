package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.*;
import br.com.dbccompany.TabalhoFinal.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentosService {
    @Autowired
    private PagamentosRepository repository;

    @Autowired
    private SaldoClientesService saldoClientesService;


    @Transactional(rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamento) {
        Contratacao contratacao = pagamento.getContratacao();
        ClientesPacotes clientesPacotes = pagamento.getClientesPacotes();
        if(contratacao != null){
            saldoClientesService.salvar(contratacao.getClientes().getId(),contratacao.getEspaco().getId(),contratacao.getTipoContratacao(),contratacao.getQuantidade(),contratacao.getPrazo());
            // verificar se existe saldo cliente com este id, se sim, verificar se está vencido, se não, vencimento = quantidade+prazo+ data do antigo vencimento
        }
        if (clientesPacotes != null){
            Clientes cliente = clientesPacotes.getCliente();
            List<EspacosPacotes> espacosPacotes = clientesPacotes.getPacote().getEspacosPacotes();
            for (EspacosPacotes espacoPacote : espacosPacotes) {
                saldoClientesService.salvar(cliente.getId(),espacoPacote.getEspaco().getId(),espacoPacote.getTipoContratacao(),espacoPacote.getQuantidade(),espacoPacote.getPrazo());
            }
        }
        return repository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos editar(Pagamentos pagamento, Integer id) {
        pagamento.setId(id);
        return this.salvar(pagamento);
    }

    public List<Pagamentos> listarTodos(){
        return (List<Pagamentos>) repository.findAll();
    }


}
