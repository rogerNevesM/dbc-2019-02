package br.com.dbccompany.TabalhoFinal.Controller;

import br.com.dbccompany.TabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.TabalhoFinal.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuariosController {
    @Autowired
    UsuariosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuarios> listarTodos(){
        List<Usuarios> usuarios = service.listarTodos();
        return usuarios;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuarios novo(@RequestBody Usuarios usuario){
        try {
            usuario = service.salvar(usuario);
            return usuario;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuarios editar(@PathVariable Integer id,@RequestBody Usuarios usuario) {
        String senha = usuario.getSenha();
        usuario.setId(id);
        try {
            usuario = service.editar(usuario, senha);
            return usuario;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
