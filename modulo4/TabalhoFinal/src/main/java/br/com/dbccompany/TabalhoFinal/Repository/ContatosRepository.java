package br.com.dbccompany.TabalhoFinal.Repository;

import br.com.dbccompany.TabalhoFinal.Entity.Contatos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatosRepository extends CrudRepository<Contatos, Integer> {
}
