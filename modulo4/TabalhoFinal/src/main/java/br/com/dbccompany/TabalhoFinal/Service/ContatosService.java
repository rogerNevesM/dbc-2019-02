package br.com.dbccompany.TabalhoFinal.Service;

import br.com.dbccompany.TabalhoFinal.Entity.Contatos;
import br.com.dbccompany.TabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.TabalhoFinal.Repository.ContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service

public class ContatosService {
    @Autowired
    private ContatosRepository repository;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Transactional(rollbackFor = Exception.class)
    public Contatos salvar(Contatos contato) {
        TipoContato tipoContato = contato.getTipoContato();
        if(tipoContato!=null && tipoContato.getId()==null){
            tipoContato = tipoContatoService.salvar(tipoContato);
        }
        contato.setTipoContato(tipoContato);
        return repository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contatos editar(Contatos contato, Integer id) {
        contato.setId(id);
        return this.salvar(contato);
    }

    public List<Contatos> listarTodos(){
        return (List<Contatos>) repository.findAll();
    }
}
