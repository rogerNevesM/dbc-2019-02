package br.com.dbccompany.TabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoCliente.class)
public class SaldoCliente {
    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;
    private Integer quantidade;
    private Date vencimento;

    @OneToMany(mappedBy = "saldoCliente",cascade = CascadeType.ALL)
    private List<Acessos> acessos = new ArrayList<>();

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }
}
